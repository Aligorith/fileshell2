#!/usr/bin/env python
#
# File browser application
# Experimental system developed for COSC460 Honours Project, 2012
#
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

from fileshell import launcher

if __name__ == '__main__':
	launcher.main()

# Export list of icons as xml string
# To be used to ensure that all necessary parts are included in resources.qrc

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os

# list of directories to traverse
dirs = ['mimetypes']

# indent level
indent = 2

# perform export process
with open('icons_export.xml', 'w') as f:
	# traverse directories, printing out necessary info per line
	for dir in dirs:
		for name in os.listdir(dir):
			f.write("%s<file>icons/%s/%s</file>\n" % ('\t'*indent, dir, name))


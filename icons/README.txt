The icons included are are from:
http://openiconlibrary.sourceforge.net/gallery2/?./Icons/mimetypes/

Specifically, the majority are the Oxygen-style ones, although a few others have been used where no suitable
versions were available from the Oxygen set.
# Time range processing algorithms
#
# These algorithms are used for processing times to cluster them according 
# to the ranges that we require.
#
# From the Python docs, we have the following magic that we can use to determine 
# individual time components from the timestamps on file data:
# 		time.localtime(time.time() => secs)  ==> struct_time(year, month, day, hr, min, sec)
# The inverse of this operation (actually, timegm not localtime) is the following:
# 		time.mktime(struct_time(...))    ==> secs 
#
# However, for actual manipulation, we need to use some representations which can actually
# be edited to give us what we need to use:
# 		datetime.datetime.fromtimestamp(time.time() => secs) ==> datetime.datetime(Y, M, D, hr, min, sec)

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import collections
import datetime
import time

##################################
# Utility Defines

# Time Deltas for Efficiency
TimeDeltas = {
	'Now'		: datetime.timedelta(minutes = 1),
	
	'Last Hour' : datetime.timedelta(hours = 1),
	'3 Hours'   : datetime.timedelta(hours = 3),
	
	'Day'       : datetime.timedelta(days = 1),
	'Yesterday' : datetime.timedelta(days = 2),
	'Week'      : datetime.timedelta(weeks = 1),
	'2 Weeks'   : datetime.timedelta(weeks = 2),
	
	'Month'     : datetime.timedelta(days = 30),  # XXX: this is a crufty assumption!
	'6 Months'  : datetime.timedelta(days = 182),
	
	'Year'      : datetime.timedelta(days = 365)
}


# Obtain datetime object from timestamp
# < timestamp: (float) seconds, as per time.time()
# > return: (datetime) editable representation
def dtFromTimestamp(timestamp):
	return datetime.datetime.fromtimestamp(timestamp)

# Obtain timestamp from datetime object
def timestampFromDt(dt):	
	return time.mktime(dt.timetuple())

###################################
# Time Range Calculator

# Time Range Calculator
# 
# Used to check whether a given timestamp falls within certain time ranges
# relative to a reference timestamp (usually the current time - i.e. when 
# the calculator was created).
#
# All methods with name "from*()", work as follows:
#	< timestamp: (float | datetime.datetime) time stamp to check
#				represented in seconds or as an object ready to be manipulated
#	> return: (bool) true if timestamp is within range
class TimeRangeCalc(object):
	__slots__ = (
		'ctime',	# (datetime) datetime to use as reference for all calculations
		'deltas'	# ({datetime}) cache of "ctime - timedelta[key]" 
	)
	
	# ctor
	# < (ctime): (datetime | float) "current" time in seconds that all other measurements are made relative to
	def __init__(self, ctime=None):
		# current time
		if ctime is None:
			self.ctime = dtFromTimestamp(time.time())
		elif type(ctime) is float:
			self.ctime = dtFromTimestamp(ctime)
		else:
			self.ctime = ctime
		
		# timestamps of current time with offsets applied for accelerating calculations
		# NOTE: we lazy-initialise these to avoid a heavy upfront cost for one-off calculations
		self.deltas = {}
	
	# Internal API ----------------------------------------
	
	# get time interval start value
	# < key: (str) see TimeDeltas.keys()
	# > returns: (datetime) the datetime 
	def get_relative_time(self, key):
		# compute value if it doesn't exist yet (lazy init)
		if key not in self.deltas:
			if key in TimeDeltas:
				delta = TimeDeltas[key]
				self.deltas[key] = self.ctime - delta
			elif key == 'Year':
				yr = self.ctime.year - 1
				self.deltas[key] = self.ctime.replace(year = yr)
			elif key == '2 Years':
				yr = self.ctime.year - 2
				self.deltas[key] = self.ctime.replace(day=0, month=0, year = yr)
			elif key == '3 Years':
				yr = self.ctime.year - 3
				self.deltas[key] = self.ctime.replace(day=0, month=0, year = yr)
			else:
				raise KeyError, "Unknown key - %s - for time interval start" % (key)
			
		# return the value from the cache
		return self.deltas[key]
		
	# check if the given timestamp occurs within the specified timeframe
	def is_in_range(self, timestamp, start, end):
		# create timestamp if it doesn't exist already
		if type(timestamp) is float:
			timestamp = dtFromTimestamp(timestamp)
			
		# get time interval start
		if type(start) is str:
			start = self.get_relative_time(start)
			
		# get time interval end
		if end is None:
			# use today/now if undefined
			end = self.ctime
		elif type(end) is str:
			# find the key for this
			end = self.get_relative_time(end)
		
		# check if timestamp exists within these boundaries
		return start <= timestamp <= end
	
	# External API ----------------------------------------
	
	# "now"
	def fromNow(self, timestamp):
		return self.is_in_range(timestamp, 'Now', self.ctime)
	
	# within past hour
	def fromLastHour(self, timestamp):
		return self.is_in_range(timestamp, 'Last Hour', self.ctime)
		
	# within past 3 hours (3 as this is size/length of standard "unit of work" sessions)
	def fromLastFewHours(self, timestamp):
		return self.is_in_range(timestamp, '3 Hours', self.ctime)
		
	# within past day
	def fromLastDay(self, timestamp):
		return self.is_in_range(timestamp, 'Day', self.ctime)
	
	
	# Period starting from yesterday to today or now. See diagram below:
	#       | HHMM                 | HHMM               | HHMM 
	#       | YesterdayStart (-2d)]| TodayStart (-1d)  ]| Now]
	#
	# < (includeToday): (bool) if true, period includes today to now, otherwise only yesterday to today
	def fromYesterday(self, timestamp, includeToday=False):
		if includeToday:
			return self.is_in_range(timestamp, 'Yesterday', self.ctime)
		else:
			return self.is_in_range(timestamp, 'Yesterday', 'Day')
	
	# within past week
	def fromLastWeek(self, timestamp):
		return self.is_in_range(timestamp, 'Week', self.ctime)
		
	# Period starting from 2 weeks to this week or now. See diagram for fromYesterday...
	def fromFortnight(self, timestamp, includeThisWeek=False):
		if includeThisWeek:
			return self.is_in_range(timestamp, '2 Weeks', self.ctime)
		else:
			return self.is_in_range(timestamp, '2 Weeks', 'Week')
			
	# within past month
	def fromLastMonth(self, timestamp):
		return self.is_in_range(timestamp, 'Month', self.ctime)
		
	# within past 6 months
	# XXX: may be better to have fine-grained filtering?
	def fromLastHalfYear(self, timestamp):
		return self.is_in_range(timestamp, '6 Months', self.ctime)
		
	
	# within past year
	def fromLastYear(self, timestamp):
		return self.is_in_range(timestamp, 'Year', self.ctime)
	
	# Period starting 2 years ago to past year or now
	def fromTwoYears(self, timestamp, includeThisYear=False):
		if includeThisYear:
			return self.is_in_range(timestamp, '2 Years', self.ctime)
		else:
			return self.is_in_range(timestamp, '2 Years', 'Year')
		
	# Period starting 3 years ago to 2 years ago or now
	def fromThreeYears(self, timestamp, includeLastTwo=False):
		if includeLastTwo:
			return self.is_in_range(timestamp, '3 Years', self.ctime)
		else:
			return self.is_in_range(timestamp, '3 Years', '2 Years')
			
	# -------------------------------------------------------------------------
	
	# Construct a "time range vector" which represents which time ranges the
	# given timestamp falls into
	# < timestamp: (float / datetime)
	# > returns: (TimeRangeVec) vector of booleans of values, from most recent to oldest
	def toTimeRangeVec(self, timestamp):
		# calculate each field
		vec = []
		for key in TimeRangeVec._fields:
			if key == 'Older':
				vec += [ not self.fromTwoYears(timestamp, True) ]
			else:
				vec += [ getattr(self, 'from%s' % (key))(timestamp) ]
		
		# return a vec like this...
		return TimeRangeVec(*vec)

##################################

# test cases 1
def do_unit_tests():
	import unittest
	
	class TestTimeRangeCalculations(unittest.TestCase):
		def setUp(self):
			self.now = datetime.datetime(2012, 6, 11, 14, 15, 16)
			self.trc = TimeRangeCalc(self.now)
			
		def test_fromLastHour(self):
			dt = datetime.datetime
			
			tests = [
				[self.now, True, 'same 1'],
				
				[dt(2012, 6, 11, 14, 00, 00), True, 'within 1'],
				[dt(2012, 6, 11, 13, 15, 16), True, 'within 2'],
				
				[dt(2012, 6, 11, 13, 15, 15), False, 'outside 1'], # less
				[dt(2012, 6, 11, 14, 15, 17), False, 'outside 2'], # greater
				[dt(2012, 6, 10, 14, 15, 16), False, 'outside 3']  # date
			]
			
			for test in tests:
				self.assertEqual(self.trc.fromLastHour(test[0]), test[1], test[2])
		
		def test_fromLastFewHours(self):
			dt = datetime.datetime
			
			tests = [
				[self.now, True, 'same 1'],
				
				[dt(2012, 6, 11, 14, 00, 00), True, 'within 1'],
				[dt(2012, 6, 11, 11, 15, 16), True, 'within 2'],   # <--- difference from last hour
				
				[dt(2012, 6, 11, 11, 15, 15), False, 'outside 1'], # less
				[dt(2012, 6, 11, 14, 15, 17), False, 'outside 2'], # greater
				[dt(2012, 6, 10, 14, 15, 16), False, 'outside 3']  # date
			]
			
			for test in tests:
				self.assertEqual(self.trc.fromLastFewHours(test[0]), test[1], test[2])
				
		def test_fromLastDay(self):
			dt = datetime.datetime
			
			tests = [
				[self.now, True, 'same 1'],
				
				[dt(2012, 6, 11,  9, 10, 11), True, 'within 1'],
				[dt(2012, 6, 10, 14, 15, 16), True, 'within 2'],
				[dt(2012, 6, 11,  0,  0,  0), True, 'within 3'],
				
				[dt(2012, 6, 10, 14, 15, 15), False, 'outside 1'], # less
				[dt(2012, 6, 11, 14, 15, 17), False, 'outside 2'], # greater
				[dt(2012, 6,  9, 14, 15, 16), False, 'outside 3'], # date
				[dt(2012, 5, 11, 14, 15, 16), False, 'outside 4']  # date
			]
			
			for test in tests:
				self.assertEqual(self.trc.fromLastDay(test[0]), test[1], test[2])
		
	# run all tests
	suite = unittest.TestLoader().loadTestsFromTestCase(TestTimeRangeCalculations)
	unittest.TextTestRunner(verbosity=1).run(suite)
	

if __name__ == '__main__':
	do_unit_tests()
	
	

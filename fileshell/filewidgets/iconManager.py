# Icon manager is responisble for maintaining a cache of
# icons for files, but also for figuring out what icon
# a particular file should get based on its extension

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

#########################################
# Global Dictionaries - Available everywhere

# Map from extensions to icon names
EXTENSION_TO_ICON_NAME = {
	# office docs
	'.pdf'    : 'application-pdf',
	
	'.doc'    : 'application-msword',
	'.docx'   : 'application-msword',
	'.xls'    : 'application-msexcel',
	'.xlsx'   : 'application-msexcel',
	'.ppt'    : 'application-mspowerpoint',
	'.pptx'   : 'application-mspowerpoint',
	
	# multimedia
	'.mid'    : 'audio-midi',
	'.midi'   : 'audio-midi',
	
	'.mp3'    : 'audio-basic',
	'.ogg'    : 'audio-basic',
	'.wav'    : 'audio-x-wav',
	
	'.bmp'    : 'image-x-generic',
	'.gif'    : 'image-x-generic',
	'.jpg'    : 'image-x-generic',
	'.jpeg'   : 'image-x-generic',
	'.png'    : 'image-x-generic',
	'.tga'    : 'image-x-generic',
	'.tif'    : 'image-x-generic',
	
	'.avi'    : 'video-x-generic',
	'.h264'   : 'video-x-generic',
	'.m4v'    : 'video-x-generic',
	'.mov'    : 'video-x-generic',
	'.mpg'    : 'video-x-generic',
	'.mpeg'   : 'video-x-generic',
	'.ogv'    : 'video-x-generic',
	'.wmv'    : 'video-x-generic',
	
	# data
	'.csv'    : 'text-csv',
	'.dat'    : 'text-csv',
	
	# blender
	'.blend'  : 'application-x-blender',
	
	# programming
	'.c'      : 'text-x-csrc',
	'.h'      : 'text-x-chdr',
	
	'.cpp'    : 'text-x-c++src',
	'.hpp'    : 'text-x-c++hdr',
	
	'.cs'     : 'text-x-csharp',
	
	'.hs'     : 'text-x-haskell',
	
	'.jar'    : 'application-x-java',
	'.java'   : 'text-x-java',
	
	'.ly'     : 'text-x-lilypond',
	
	'Makefile': 'text-x-makefile',
	'.make'   : 'text-x-makefile',
	
	'.py'     : 'text-x-python',
	
	'.xml'    : 'text-xml',
	
	# tex...
	'.tex'    : 'text-x-tex',
	'.bib'    : 'text-x-bibtex',
	
	# web...
	'.html'   : 'text-html',
	'.htm'    : 'text-html',
	'.php'    : 'application-x-php',
	'.js'     : 'application-javascript',
	'.css'    : 'text-plain',	# XXX: there should be a text-css, but we don't have icon for that
	
	# archives
	'.7z'     : 'application-x-archive',
	'.bz2'    : 'application-x-archive',
	'.gzip'   : 'application-x-archive',
	'.tar'    : 'application-x-archive',
	'.tar.gz' : 'application-x-archive',
	'.zip'    : 'application-x-archive',
	
	# executables
	'.exe'    : 'application-x-executable',
	'.bat'    : 'text-x-script',
	'.sh'     : 'text-x-script',
	
	# general
	'.txt'    : 'text-plain',
	
	'?'       : 'file'			# catch-all for all unknown
	#'?'       : 'unknown'			# catch-all for all unknown
}

#########################################
# Dynamically Computed Global Dictionaries

# SVG icon instances (as QIcons) for each mimetype identified above
MIME_QICON_CACHE = {}

# Icons cached by size and active/disabled status
# Levels:
#   1) L1: QIcon instance
#      2) Size: (48, 64, etc.)
#         3) Active/Inactive
ICON_CACHE = {}

#########################################
# API for accessing icons

# Find icon for a given filename
# < fileN: (str)
# > returns: (QIcon)
def findBaseIcon(fileN):
	global EXTENSION_TO_ICON_NAME, MIME_QICON_CACHE
	
	# lookup based on extension
	ext = os.path.splitext(fileN)[1]
	
	if ext in EXTENSION_TO_ICON_NAME:
		# known value
		mimetype = EXTENSION_TO_ICON_NAME[ext]
	else:
		# unknown/generic
		mimetype = EXTENSION_TO_ICON_NAME['?']
		
	# initialise cache on first use, as we cannot do this init on module level
	# since QApp doesn't exist yet when this module gets imported
	if len(MIME_QICON_CACHE) == 0:
		MIME_QICON_CACHE = dict([ (name, qgui.QIcon(":/icons/mimetypes/%s.svg" % (name)) )
									for name in EXTENSION_TO_ICON_NAME.values() ])
	
	# fetch from cache 
	return MIME_QICON_CACHE[mimetype]

# Tint color of icon
# < icon_pixmap: (QIcon | QPixmap) representation of icon to tin
# < size: (int) size of icon in pixels - 48 is a fairly nice value (for large), and 16 (for small)
# < tint_color: (QColor) colour to apply
# < (strength): (float) how strongly should tint be applied - from domain
# < (max_strength): (float) extent to which tint can be applied - standard constant
# > returns: (QPixmap) a copy of the icon with tinting applied
def iconApplyTint(icon, size, tint_color, strength = 1.0, max_strength = 0.7):
	# construct a temp buffer to combine the results in
	#temp = qgui.QPixmap(icon_pixmap)
	if isinstance(icon, qgui.QIcon):
		temp = icon.pixmap(size, size)
	else:
		temp = qgui.QPixmap(icon)
		# XXX: need way to resize...
	
	
	# set mask in new pixmap - this ensures that we only paint within the bounds of the image
	# which in our case we get from converting the existing image
	mask = temp.createMaskFromColor(qgui.QColor("white"), qcore.Qt.MaskInColor)  # XXX: assumes that background is always white
	temp.setMask(mask)
	
	# use a painter to apply the needed color tint
	col = qgui.QColor(tint_color)
	col.setAlpha(col.alpha() * strength * max_strength)
	
	painter = qgui.QPainter(temp)
	painter.setCompositionMode(painter.CompositionMode_SourceAtop)
	
	painter.fillRect(temp.rect(), col)
	
	painter.end()
	
	# return the tinted icon
	return temp

# Load relevant icon from the cache or create it
# < base: (QIcon)
# < size: (int) size of icon in pixels - 48 is a fairly nice value
# < state: (qgui.QIcon.Normal | qgui.QIcon.Disabled)
def getDisplayIcon(base, size, state):
	global ICON_CACHE
	
	# look up each level of the cache, creating if non-existent
	if not base in ICON_CACHE:
		ICON_CACHE[base] = {}
	if not size in ICON_CACHE[base]:
		ICON_CACHE[base][size] = {}
	if not state in ICON_CACHE[base]:
		ICON_CACHE[base][size][state] = base.pixmap(size, size, state)
	
	# return image
	return ICON_CACHE[base][size][state]

#########################################

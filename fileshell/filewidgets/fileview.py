# File grid/list widget
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
import subprocess

import itertools
from math import sqrt
import random

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.model import fsdb
from fileshell.model.fsutils import *
from fileshell.time.timeRange import *

from fileshell.theming import stylesheets
from fileshell.filewidgets import iconManager as ICM

##########################################
# User-defined list item custom data (starts from 32) items

# "Link to original item" 
ItemPtrRole = 33

# Filetype icon (QIcon) reference
# Used for getting the active/inactive versions of an image's values
BaseIconRole = 34

# "Double-click target" (for redirects)
OpenPtrRole = 35

############################################
# Adjacent-Folder Navigation Buttons

# Event filter for "Next/Prev Neighbour" buttons to dynamically build tooltips
# to include contextual information relevant to figuring out where it'd take you
class NeighbourNav_EventFilter(qcore.QObject):
	# ctor
	# < parent: (QObject) ???
	# < navmodel: (NavModel) current navigation model used
	# < dir: (-1 | 1) prev or next
	def __init__(self, parent, navmodel, dir, text):
		qcore.QObject.__init__(self, parent)
		
		# settings for building this info
		self.navmodel = navmodel
		self.dir = dir
		self.text = text
		
		# cache for target item
		self.target = None # (FolderData)  
		
	# cache new neighbour value - called on each location change
	def updateStatus(self):
		# query folder that we'll be going to...
		nm  = self.navmodel
		self.target = nm.find_sibling(nm.location, nm.depth, self.dir)
		
	# handle event
	def eventFilter(self, obj, event):
		if event.type() == qcore.QEvent.ToolTip:
			# query folder that we'll be going to...
			dest = self.target
			
			# set tooltip accordingly
			if dest:
				if dest.parent and dest.parent():
					destStr = "%s [%s]" % (dest.name, dest.parent().name)
				else:
					destStr = "%s (<i>Root</i>)" % (dest.name)
			else:
				destStr = "<i>No neighbours found over this way...</i>"
					
			msg = "<p style='white-space:pre'><b>%s</b><br />%s</p>" % (destStr, self.text)
			qgui.QToolTip.showText(event.globalPos(), msg)
				
			# done
			return True
		
		# not done
		return False

############################################
# File List View - Standard List version

# Basic/standard list widget representation of file list
class StandardFileList(qgui.QListWidget):	
	# Custom event for updating highlights in SCOFT (i.e. folder previewing intent)
	# < index: (int) display-model index for folder
	folderHighlightChanged = qcore.pyqtSignal(int)
	
	# Custom event for showing tag in SCOFT (i.e. post-nav)
	# < index: (int) display-model index for folder
	postItemOpen = qcore.pyqtSignal(int)
	
	# -----------------------------------
	
	# ctor
	def __init__(self, fs_model, navmodel, filters, features):
		qgui.QListWidget.__init__(self)
		
		# set default view settings
		# XXX: needs review
		self.setViewMode(qgui.QListView.IconMode)
		self.setResizeMode(qgui.QListView.Adjust)
		self.setLayoutMode(qgui.QListView.Batched)
		self.setFlow(qgui.QListView.LeftToRight)
		self.setMovement(qgui.QListView.Snap)
		
		self.setGridSize(qcore.QSize(128, 100))
		#self.setUniformItemSizes(True)
		self.setSpacing(20)
		
		self.setWordWrap(True)
		self.setTextElideMode(qcore.Qt.ElideMiddle) # core assumption: middle part is padding fluff
		
		self.setMouseTracking(True)
		
		# store references to models
		self.fs_model = fs_model
		self.navmodel = navmodel
		self.filters = filters
		
		# current set of list item widgets - for easier updating
		self.folder_items = []
		self.file_items = []
		
		# next/prev overlay buttons
		self.setup_ui()
		
		# bind event handling
		self.setup_shortcuts()
		self.bind_events()
		
	# bind shortcut keys
	def setup_shortcuts(self):
		# I'm feeling lucky (or bored ;)
		randomNaviShortcut = qgui.QShortcut(qgui.QKeySequence("Shift+B"), self)
		randomNaviShortcut.activated.connect(self.on_RandomNavigation)
		
	# bind events
	def bind_events(self):
		# model updates
		self.navmodel.locationChanged.connect(self.build_list)
		self.navmodel.locationChanged.connect(self.update_sibling_buts)
		
		self.filters.filtersChanged.connect(self.update_filters)
		
		# own events
		self.itemDoubleClicked.connect(self.on_ItemDoubleClick)
		
		# list widget
		# - hover event
		self.entered.connect(self.on_IndexHovered)
		
	# Widgets =============================
	
	def setup_ui(self):
		# FIXME: tooltips on these should be dynamic
		# FIXME: the stylesheet causes flickering on updates
		
		# size of icons for buttons
		iconSize = qcore.QSize(24, 48)
		
		# previous button -------------------------------------------------------------
		self.prevButHandler = NeighbourNav_EventFilter(self, self.navmodel, -1, 
														"Previous Neighbour")
		
		self.prevBut = qgui.QPushButton()
		self.prevBut.installEventFilter(self.prevButHandler)
		#self.prevBut.setToolTip("Go to Neighbour (Previous)")
		
		self.prevBut.setObjectName("PrevSiblingBut")
		self.prevBut.setStyleSheet(stylesheets.LeftRightViewButs)
		
		self.prevBut.setIcon(qgui.QIcon(":/images/side-arrow-left.png"))
		self.prevBut.setIconSize(iconSize)
		
		self.prevBut.clicked.connect(self.navmodel.goPrev)
		
		# next button ------------------------------------------------------------------
		self.nextButHandler = NeighbourNav_EventFilter(self, self.navmodel, 1, 
														"Next Neighbour")
		
		self.nextBut = qgui.QPushButton()
		self.nextBut.installEventFilter(self.nextButHandler)
		#self.nextBut.setToolTip("Go to Neighbour (Next)")
		
		self.nextBut.setObjectName("NextSiblingBut")
		self.nextBut.setStyleSheet(stylesheets.LeftRightViewButs)
		
		self.nextBut.setIcon(qgui.QIcon(":/images/side-arrow-right.png"))
		self.nextBut.setIconSize(iconSize)
		
		self.nextBut.clicked.connect(self.navmodel.goNext)
		
		# layout management ----------------------------------------------------------
		
		hbox = qgui.QHBoxLayout()
		self.setLayout(hbox)
		
		hbox.addWidget(self.prevBut, stretch=0)
		hbox.addStretch(1)
		hbox.addWidget(self.nextBut, stretch=0)
		
		hbox.setSpacing(0)
		hbox.setMargin(0)
		hbox.setContentsMargins(0, 0, 10, 0) # to account for scrollbar, only on left
		self.setViewportMargins(20, 0, 0, 0) # to balance out whitespace on edge, and to avoid collisions
		self.setStyleSheet("QScrollArea, QAbstractScrollArea { background-color: palette(base);}") # XXX: returns the refresh bugs
		
	# List Building =======================
	
	# Rebuild list (in response to location change)
	# TODO: this process should be executed in a background thread?
	def build_list(self):
		# clear existing items
		self.folder_items = []
		self.file_items = []
		
		self.clear()
		
		# grab the folder for convenience
		loc = self.navmodel.location
		if loc is None: return;
		
		# grab common icon for folders
		# XXX: this should eventually get added to icon manager...
		folderIcon = qgui.QIcon.fromTheme("folder", qgui.QIcon(":/images/folder.svg"));
		
		# item size constraint
		itemSize = qcore.QSize(120, 90)
		
		# add new child widgets
		for folder in loc.subdirs:
			# create and add item
			item = qgui.QListWidgetItem(folderIcon, folder.name)
			item.setSizeHint(itemSize)
			
			self.folder_items.append(item)
			self.addItem(item)
			
			# reference folder
			item.setData(ItemPtrRole, folder)
			
			# base icon
			item.setData(BaseIconRole, folderIcon)
			
			# tooltip
			item.setData(qcore.Qt.ToolTipRole, self.generate_tooltip(folder))
		
		#self.repaint() # redraw to look alive
			
		for file in loc.files:
			# create and add item
			file_icon = ICM.findBaseIcon(file.name)
			item = qgui.QListWidgetItem(file_icon, file.name)
			item.setSizeHint(itemSize)
			
			self.file_items.append(item)
			self.addItem(item)
			
			# reference file
			item.setData(ItemPtrRole, file)
			
			# base icon
			item.setData(BaseIconRole, file_icon)
			
			# tooltip
			item.setData(qcore.Qt.ToolTipRole, self.generate_tooltip(file))
		
		#self.repaint() # redraw to look alive
		
		# ensure we scroll back up to first item
		if self.folder_items:
			self.scrollToItem(self.folder_items[0])
		elif self.file_items:
			self.scrollToItem(self.file_items[0])
		
		# update filters
		self.update_filters()
		self.repaint() # redraw to look alive
		
	
	# Helper function to generate tooltip for a FsItem
	def generate_tooltip(self, fitem):
		# TODO: do some smart displays which extract out the "salient" features of the path
		tooltip  = "<p style='white-space:pre'>"
		tooltip += "<b>%s</b> <br />" % (fitem.name)
		
		if isinstance(fitem, fsdb.FolderData):
			tooltip += "<i>Folder</i>"
		else:
			fileType = "File"
			tooltip += "<i>%s</i>" % (fileType)
		tooltip += "<br /><br />"
		
		if 'm' in fitem.attributes:
			# XXX: folders should be "created" time?
			ts = time.localtime(fitem.attributes['m'])
			tstr = time.strftime("%d %b %Y   %I:%M %p", ts) 
			
			tooltip += "<i>Modified:</i> %s <br />" % (tstr)
		
		if isinstance(fitem, fsdb.FolderData):
			# number of children + subfolders
			dstat = "%s Folder%s" % (len(fitem.subdirs), '' if len(fitem.subdirs)==1 else 's')
			fstat = "%s File%s"   % (len(fitem.files),   '' if len(fitem.files)==1   else 's')
			
			tooltip += "<i>Contains:</i> %s, %s" % (dstat, fstat)
		else:
			# size?
			if 's' in fitem.attributes:
				# TODO: format this!
				size = fitem.attributes['s'] # bytes
				unit = 'bytes'
				
				if size > 1000: 
					size /= 1000 # bytes -> kb
					unit = 'kb'
					
				if size > 1000:
					size /= 1000 # kb -> mb
					unit = 'mb'
				
				tooltip += "<i>Size:</i> %s %s" % (size, unit)
		
		tooltip += "</p>"
		return tooltip
	
	# Update enabled status of next/prev neighbour buttons
	def update_sibling_buts(self):
		# update each handler in turn
		self.prevButHandler.updateStatus()
		self.nextButHandler.updateStatus()
		
		# update enabled/disabled status in response to whether there's a target
		self.prevBut.setEnabled(self.prevButHandler.target is not None)
		self.nextBut.setEnabled(self.nextButHandler.target is not None)
	
	# Update filtering of items
	def update_filters(self):
		# preset colors
		# XXX: should probably fetch these from palette
		relevantHilight = qgui.QColor(245, 121, 0) # <-- see fishprints.py
		relevantColor = qgui.QColor("black")
		
		bookmarkHilight = qgui.QColor(210, 20, 0)  # <-- see fishprints.py
		
		if os.name == 'posix':
			# Hack: Linux needs slightly darker shades, or else the items can't be seen!
			irrelevantCol = qgui.QColor("gray")
		else:
			irrelevantCol = qgui.QColor("lightgray")
			
		# get base icon...
		baseIcon = (lambda it: it.data(BaseIconRole).toPyObject())
		
		# size of icons
		SIZE = 48
		
		# folders
		for ditem in self.folder_items:
			# based on binary state of is/is-not in range (i.e. whether it satisfies the filter), adjust display state
			folder = self.extract_item_data(ditem)
			if self.filters.folderOkOverview[folder]:
				# enabled
				icon = ICM.getDisplayIcon(baseIcon(ditem), SIZE, qgui.QIcon.Normal)
				if folder in self.filters.importantFolders:
					if folder.bookmarked:
						folderColHint = bookmarkHilight
					else:
						folderColHint = relevantHilight
						
					icon = ICM.iconApplyTint(icon, SIZE, folderColHint, 
											 strength=self.filters.folderColorHints[folder])
				
				ditem.setData(qcore.Qt.DecorationRole, icon)
				ditem.setData(qcore.Qt.ForegroundRole, relevantColor)
			else:
				# disabled
				icon = ICM.getDisplayIcon(baseIcon(ditem), SIZE, qgui.QIcon.Disabled)
				#if folder in self.filters.importantFolders:
				#	icon = ICM.iconApplyTint(icon, SIZE, self.filters.folderColorHints[folder])
								
				ditem.setData(qcore.Qt.DecorationRole, icon)
				ditem.setData(qcore.Qt.ForegroundRole, irrelevantCol)
			
		# files		
		for fitem in self.file_items:
			# text color and icon
			# - based on binary state of is/is-not in range, and whether it satisfies the filter
			file = self.extract_item_data(fitem)
			if self.filters.itemCanShow(file):
				fitem.setData(qcore.Qt.ForegroundRole, relevantColor)
				fitem.setData(qcore.Qt.DecorationRole, ICM.getDisplayIcon(baseIcon(fitem), SIZE, qgui.QIcon.Normal))
			else:
				fitem.setData(qcore.Qt.ForegroundRole, irrelevantCol)
				fitem.setData(qcore.Qt.DecorationRole, ICM.getDisplayIcon(baseIcon(fitem), SIZE, qgui.QIcon.Disabled))
		
	# Methods =============================
	
	# extract file/folder from item
	def extract_item_data(self, item):
		return item.data(ItemPtrRole).toPyObject()
		
	# get "active" item - first selected
	def get_active_item(self):
		selection = self.selectedItems()
		item = selection[0] if len(selection) else None
		
		return item
		
	# get folder - current location or selected
	# < (use_selected): (bool) whether the selected item in the view is used instead of the folder/location
	def get_folder(self, use_selected=False):
		# get path of item to copy
		if use_selected:
			listItem = self.get_active_item()
			if listItem:
				return self.extract_item_data(listItem)
			else:
				return None
		else:
			# current folder
			return self.navmodel.location
	
	# Context Menus ========================
	
	# Context menu
	def contextMenuEvent(self, event):
		# position for spawning event from
		mco = self.mapToGlobal(event.pos())
		
		# current folder, selected folder, or file menu?
		listItem = self.get_active_item()
		
		if listItem:
			# folder or file?
			item = self.extract_item_data(listItem)
			
			if isinstance(item, fsdb.FolderData):
				self.contextMenu_folder(mco, use_selected=True)
			else:
				self.contextMenu_file(mco)
		else:
			# current folder
			self.contextMenu_folder(mco)
			
	# Context menu for folder
	def contextMenu_folder(self, mco, use_selected=False):
		folder = self.get_folder(use_selected)
		
		# create the menu --------------------------------
		# TODO: add all actions in advance, and have them get all their events hooked up in advance too
		#       then we just load them in here...
		menu = qgui.QMenu(self)
		
		# 1) Open in Command Prompt
		actOpenConsole = menu.addAction(qgui.QIcon(":/images/console.svg"),
										"Open in Console")
		if use_selected:
			actOpenConsole.triggered.connect(lambda: self.on_OpenConsole(use_selected=True))
		else:
			actOpenConsole.triggered.connect(self.on_OpenConsole)
		menu.setDefaultAction(actOpenConsole)
		
		actRandomSelect = menu.addAction("I'm Feeling Lucky...")
		actRandomSelect.setToolTip("Open a randomly selected item (<i>Shift-B</i>)")
		actRandomSelect.triggered.connect(self.on_RandomNavigation)
		
		menu.addSeparator()
		
		# 2) Tagging operations
		# - bookmarking
		if folder.bookmarked:
			# XXX: needs dedicated icon
			actBookmarkRemove = menu.addAction(qgui.QIcon(":/images/add_bookmark.png"),
											   "Remove Bookmark...")
			actBookmarkRemove.triggered.connect(lambda: self.on_RemoveBookmark(use_selected=use_selected))
		else:
			actBookmarkAdd = menu.addAction(qgui.QIcon(":/images/add_bookmark.png"), 
										 "Add Bookmark...")
			actBookmarkAdd.triggered.connect(lambda: self.on_AddBookmark(use_selected=use_selected))
		
		# - ignoring items
		actIgnore = menu.addAction(qgui.QIcon(":/images/folder_ignore.png"),
									 "Ignore...")
		actIgnore.triggered.connect(lambda: self.on_IgnoreSubtree(use_selected=use_selected))
							
							
		menu.addSeparator()
		
		
		# 2) Version Control
		#menu.addSeparator()
		
		# TODO: 
		#	- svn
		# 	- git
		#	- hg
		
		menu.addSeparator()
		
		# 3) Folder Tools
		actCopyPath = menu.addAction("Copy Folder Path")
		if use_selected:
			actCopyPath.triggered.connect(self.on_CopyPath)
		else:
			actCopyPath.triggered.connect(lambda: self.on_CopyPath(use_selected=False))
		
		actRefresh = menu.addAction("Refresh")
		# XXX...
		
		actPackZip = menu.addAction(qgui.QIcon(":/images/folder_tar.png"),
									"Pack as '%s.zip'" % (self.navmodel.location.name))
		# XXX...
		
		menu.addSeparator()
		
		actMoveToTrash = menu.addAction("Move to Trash")
		# XXX...
		
		actDelete = menu.addAction("Delete...")
		# XXX...
		
		actRename = menu.addAction("Rename")
		# XXX...
		
		menu.addSeparator()
		
		# 4) Properties
		actProps = menu.addAction("Properties...")
		actProps.triggered.connect(lambda: self.on_ShowProperties(use_selected=use_selected))
		
		# show menu -----------------------------------------
		
		res = menu.exec_(mco)
	
	# Context menu for file
	def contextMenu_file(self, mco):
		# create the menu --------------------------------
		# TODO: add all actions in advance, and have them get all their events hooked up in advance too
		#       then we just load them in here...
		menu = qgui.QMenu(self)
		
		# 1) Open Item
		actOpenItem = menu.addAction("Open")
		# XXX...
		menu.setDefaultAction(actOpenItem)
		
		menu.addSeparator()
		
		# 2) File Tools 
		actCopyPath = menu.addAction("Copy Folder Path")
		actCopyPath.triggered.connect(self.on_CopyPath)
		
		menu.addSeparator()
		
		actMoveToTrash = menu.addAction("Move to Trash")
		# XXX...
		
		actDelete = menu.addAction("Delete...")
		# XXX...
		
		actRename = menu.addAction("Rename")
		# XXX...
		
		menu.addSeparator()
		
		# 3) Properties
		actProps = menu.addAction("Properties...")
		actProps.triggered.connect(lambda: self.on_ShowProperties(use_selected=True))
		
		# show menu -----------------------------------------
		
		res = menu.exec_(mco)
	
	# Event Callbacks =====================
	
	# Open console
	def on_OpenConsole(self):
		# get directory to open to
		cwd = self.navmodel.location.path(fullPath=True, asStr=True)
		
		# open terminal
		if sys.platform.startswith('darwin'):
			#subprocess.call(('open', path), cwd=cwd)
			pass
		elif os.name == 'nt':
			subprocess.Popen('start cmd', cwd=cwd, shell=True)
		elif os.name == 'posix':
			#subprocess.call(('xdg-open', path), cwd=cwd)
			pass
			
	# Callback for ignoring a subtree
	def on_IgnoreSubtree(self, use_selected=True):
		folder = self.get_folder(use_selected)
		if folder is None:
			return;
		
		# ignore folder and its subtree...
		#print "Ignoring: '%s' @ '%s'" % (folder.name, folder.path(asStr=True))
		folder.ignoreTree()
		self.filters.filtersChanged.emit()
	
	# Callback for adding bookmark
	def on_AddBookmark(self, use_selected=True):
		folder = self.get_folder(use_selected)
		if folder is None:
			return;
		
		folder.itemBookmark(True)
		self.filters.filtersChanged.emit() # XXX
		
	# Callback for removing bookmark
	def on_RemoveBookmark(self, use_selected=True):
		folder = self.get_folder(use_selected)
		if folder is None:
			return;
		
		folder.itemBookmark(False)
		self.filters.filtersChanged.emit() # XXX
			
	# Copy path
	def on_CopyPath(self, use_selected=True):
		# get path of item to copy
		if use_selected:
			listItem = self.get_active_item()
			if listItem:
				item = self.extract_item_data(listItem)
			else:
				return;
		else:
			# current folder
			item = self.navmodel.location
			
		# get path of this item
		cwd = item.path(fullPath=True, asStr=True)
		
		# tack it onto the clipboard
		clipboard = qgui.QApplication.clipboard()
		clipboard.setText(cwd)
	
	# Show Properties for Item
	def on_ShowProperties(self, use_selected=True):
		# get item to show properties for
		if use_selected:
			listItem = self.get_active_item()
			if listItem:
				item = self.extract_item_data(listItem)
			else:
				return;
		else:
			item = self.navmodel.location
 			
		# for now, display message box showing the ID of the item
		if item:
			msg  = self.generate_tooltip(item)
			msg += "<hr /><p><b>Item ID:</b> %s</p>" % (item.id) 
			qgui.QMessageBox.information(self, "Properties - %s" % (item.name), msg)
		else:
			print "ERROR: invalid list item..."
	
	# Handle random navigation feature
	def on_RandomNavigation(self):
		# get the current location
		# - randomly pick between favouring files and folders
		folder = self.navmodel.location
		if (random.random() >= 0.5) and len(folder.files):
			startIdx = len(folder.subdirs)
			numItems = len(folder.files)
		else:
			startIdx = 0
			numItems = self.count()
		
		# randomly pick and index, grab the item there, and proceed to "open" it using double-click
		index = startIdx + random.randrange(numItems)
		item = self.item(index)
		
		if item:
			self.on_ItemDoubleClick(item)
		else:
			print "Doh! Couldn't select..."
	
	# Allow clicking in blank space to deselect all
	# XXX: this really depends on button used?
	def mousePressEvent(self, event):
		self.clearSelection()
		qgui.QListWidget.mousePressEvent(self, event)
		
	# Handle item double clicking
	def on_ItemDoubleClick(self, item):
		# grab file/folder that this represents
		if not item: return
		
		fitem = self.extract_item_data(item)
		if not fitem: return
		
		# type-specific handling
		if isinstance(fitem, fsdb.FolderData):
			# this is a folder 
			# - if it's the root of chain, head down
			# - but stop if we can't go any further
			while fitem.isChainLink() and fitem.subdirs:
				fitem = fitem.subdirs[0] # since just a single child
			
			# navigate to the folder at the end of the chain
			self.navmodel.setLocation(fitem)
		elif isinstance(fitem, fsdb.FileData):
			# get file path
			path = fitem.path(fullPath=True, asStr=True)
			
			# get the display index for the parent folder
			fileOwnerIndex = self.filters.folderDisplayIndices[fitem.parent()]
			
			# get current directory (so that the application can be launched relative to that)
			cwd = self.navmodel.location.path(fullPath=True, asStr=True)
			
			# mark item as "visited"
			# XXX: for now, just assume that access == modification, since all code uses mtime as "time" indicator
			oldTMax = self.fs_model.time_model.tmax
			now     = time.time() 
			
			fitem.itemModified(now)
			self.fs_model.accessItem(fitem, 'open')
			
			# force redrawing...
			# - only do a partial update if the time delta is within a day or two
			#   (86400 = 60 sec * 60 min * 24 hrs)
			if now - oldTMax < 86400:
				# partial update - times were recently changed, so revisitation change rate should be quite small still
				self.filters.refreshRankings.emit(fileOwnerIndex)
			else:
				# full update - this is quite heavyhanded
				self.filters.filtersChanged.emit()
			
			# NOTE: since this is a file not a folder, this won't indicate the location
			# on SCOFT, so let's do something about that
			self.postItemOpen.emit(fileOwnerIndex)
			
			# don't try to open/access it if this is just a snapshot
			if not self.fs_model.foreign:
				# check that it exists
				if os.path.exists(path):
					# open it
					# - as per: http://stackoverflow.com/questions/434597/open-document-with-default-application-in-python
					if sys.platform.startswith('darwin'):
						subprocess.call(('open', path), cwd=cwd)
					elif os.name == 'nt':
						# temporarily change to the directory of the file to open it, then jump back out
						# to where we were before...
						_cwd = os.getcwd()
						os.chdir(cwd)
						
						os.startfile(path)
						
						os.chdir(_cwd)
					elif os.name == 'posix':
						subprocess.call(('xdg-open', path), cwd=cwd)
				else:
					# complain
					qgui.QMessageBox.critical(self, "Open File...", 
									"File Browser could not find the requested file at:\n %s" % (path))
			else:
				print path
				
	# Handle hovering over a list item
	# < index: (QModelIndex)
	def on_IndexHovered(self, index):
		# from modelindex, get actual item
		item = self.itemFromIndex(index)
		fitem = self.extract_item_data(item)
		
		# handle different data types
		# - on hover, we should mainly trigger highlighting of items in SCOFT view
		if not fitem:
			# no item - disable highlight
			self.folderHighlightChanged.emit(-1)
		elif isinstance(fitem, fsdb.FolderData):
			# a folder
			self.folderHighlightChanged.emit(self.filters.folderDisplayIndices[fitem])
		elif isinstance(fitem, fsdb.FileData):
			# get file's folder...
			self.folderHighlightChanged.emit(self.filters.folderDisplayIndices[fitem.parent()])
				
############################################

# Folder tree-list widget
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from math import sqrt, sin, pi

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.model import fsdb
from fileshell.model.fsutils import *
from fileshell.time.timeRange import *

from fileshell.components import tagLabels
from fileshell.filewidgets import iconManager as ICM

##########################################

# "Index of Visualisation-Item" (int) - User-defined tree item custom data (starts from 32)
IOV = 33

# "Link to original folder" (FolderData) - User-defined tree item custom data (starts from 32)
FolderPtrRole = 34

# "Item is Relevant" (bool) - User-defined tree item custom data (starts from 32)
ItemRelevanceRole = 35

##########################################
# Folder Tree

# Tree widget for dipslaying the folder hierarchy tree
#
# We're being lazy here and just hacking the QTreeWidget
# (i.e. the full tree that takes care of all the model-maintaining
# itself) and adding on our own bits to make things work.
# There's not enough time now to do a proper/full Model-View split
class FsFolderTree(qgui.QTreeWidget):
	# ctor
	# < fs_model: (FsModel)
	# < filters: (FilterModel) filters used for filtering the tree
	# < features: (dict)
	def __init__(self, fs_model, filters, features):
		qgui.QTreeWidget.__init__(self)
		
		# customise tree settings so that they behave as we want
		#self.setAnimated(True)
		self.setUniformRowHeights(True)
		self.setExpandsOnDoubleClick(True)
		
		self.setIndentation(12)
		
		self.setColumnCount(1)
		self.setHeaderHidden(True)
		
		# store model links
		self.fs_model = fs_model
		self.filters = filters
		
		self.features = features
		
		# usability analysis hook - records statistics of tree events
		self.logger = None
		
	# ==========================================
	
	# update internal model used
	def rebuildModel(self):
		print "Rebuilding Folder Tree..."
		
		if self.logger: self.logger.logging = False
		
		# clear the existing set of folders
		self.tree_roots = []
		self.tree_list = []
		self.filters.folderOkOverview = {}
		
		# rebuild tree		
		for folder in self.fs_model.tree:
			self.build_tree(folder, None)
		
		# hook up decorations
		self.updateModel()
		
		if self.logger: self.logger.logging = True
		
	# helper to recursively add children
	# TODO: consider adding items layer by layer only
	def build_tree(self, folder, parent):
		# create item for folder
		item = qgui.QTreeWidgetItem([folder.name])
		
		# tooltip
		# NOTE: this only really changes when something about the folder structure changes
		item.setData(0, qcore.Qt.ToolTipRole, 
					 folder.path(fullPath=False, asStr=True))
		
		# link to folder
		item.setData(0, FolderPtrRole, folder)
		
		# add folder item to our tree
		# NOTE: this must be done before expansion settings can be set
		if parent is not None:
			parent.addChild(item)
		else:
			self.addTopLevelItem(item)
			self.tree_roots.append(item)
		
		# also add it to our list of items so that we can map the indices
		self.tree_list.append(item)
		# and finally, to the filters filtering map too...
		self.filters.folderOkOverview[folder] = True
		
		# only expand items (once added) if:
		# - this is the root item (all children should be easily accessed...
		# - we only have one child (saves clicking through to open these - el cheapo hierarchy flattening)
		#   and we aren't at one level in from the root (otherwise, the inconsistency is annoying)
		if self.features.get('use_jumps', True):
			if ( (parent is None) or 
				 (folder.isChainLink() and (parent.parent() is not None)) ):
				# expand me!
				item.setExpanded(True)
		
		# add children
		for subfolder in folder.subdirs:
			self.build_tree(subfolder, item)
	
	# Update model displayed
	# < (index): (int) display model index for folder; if provided, only update the specified item
	def updateModel(self, index = -1):
		#print "Updating FolderTree..."
		
		# some preset icons
		# NOTE: here we use the raster ones, since they read better for this format
		folderIcon = qgui.QIcon(":/images/folder.png")
		rootIcon = qgui.QIcon(":/images/folder_home.png")
		
		ignoreIcon = qgui.QIcon(":/images/folder_ignore.png")
		bookmarkIcon = qgui.QIcon(":/images/add_bookmark.png")
		
		# preset colors
		# XXX: should probably fetch these from palette
		relevantHilight = qgui.QColor(245, 121, 0) # <-- see fishprints.py
		relevantColor = qgui.QColor("black")
		
		if os.name == 'posix':
			# Hack: Linux needs slightly darker shades, or else the items can't be seen!
			irrelevantCol = qgui.QColor("gray")
		else:
			irrelevantCol = qgui.QColor("lightgray")
			
		# size of icons - small
		SIZE = 16
		
		# what items to update?
		if 0 <= index < len(self.tree_list):
			tree_item = self.tree_list[index]
			items = [tree_item]
		else:
			items = self.tree_list
		
		
		# update display of each item
		for item in items:
			folder = self.folder_from_item(item)
			
			# icon
			# XXX: find icons for "special folders" if we know anything about them
			if folder.ignore:
				# ignored item
				icon = ignoreIcon
			else:
				# visible item...
				if item.parent():
					# normal folder
					baseIcon = folderIcon
				else:
					# root folder
					baseIcon = rootIcon
					
				# apply relevance tints to icon...
				if folder in self.filters.importantFolders:
					icon = ICM.iconApplyTint(baseIcon, SIZE, relevantHilight, 
											 strength=self.filters.folderColorHints[folder])
				else:
					icon = baseIcon
			item.setData(0, qcore.Qt.DecorationRole, icon)
			
			# text color 
			# - based on binary state of is/is-not in range, and whether it satisfies the filter
			if self.filters.folderOkOverview[folder]:
				# tag item
				item.setData(0, qcore.Qt.ForegroundRole, relevantColor)
				item.setData(0, ItemRelevanceRole, True)
			else:
				item.setData(0, qcore.Qt.ForegroundRole, irrelevantCol)
				item.setData(0, ItemRelevanceRole, False)
	
	# ============================================
	
	# Get the index of a specified tree item
	def itemIndex(self, item):
		return self.tree_list.index(item)
	
	# Find the tree item with the specified index
	# < index: (int) index within flat list of item
	def findItemFromIndex(self, index):
		if abs(index) < len(self.tree_list):
			return self.tree_list[index]
		else:
			print "Item not found"
			return None
		
	# ============================================
	
	# extract folder from index
	def folder_from_item(self, item):
		return item.data(0, FolderPtrRole).toPyObject()
	
	# get folder under mouse
	def findFolderAtPoint(self, pt):
		# get item under cursor
		mindex = self.indexAt(pt)
		if not mindex.isValid():
			print "Invalid index to find folder at"
			return None
		
		item = self.itemFromIndex(mindex)
		if not item:
			print "Couldn't find item from index"
			return None
		
		# extract the folder that this refers to
		return self.folder_from_item(item)
		
	# get folder from selections - current folder selection should have changed...
	def currentFolder(self):
		item = self.selectedItems()[0]
		if item is None:
			return None
			
		return self.folder_from_item(item)
	
	# ============================================
	
	# show context menu with operations for folder here
	def contextMenuEvent(self, event):
		# only continue if it's going to end up on a folder
		folder = self.findFolderAtPoint(event.pos())
		if folder is None: return;
		
		# create the menu --------------------------------
		# TODO: add all actions in advance, and have them get all their events hooked up in advance too
		#       then we just load them in here...
		menu = qgui.QMenu(self)
		
		# 1) open and open in tab
		actOpen = menu.addAction("Open")
		menu.setDefaultAction(actOpen)
		
		actOpenTab = menu.addAction("Open in New Tab")
		actOpenTab.triggered.connect(self.on_OpenNewTab)
		
		
		menu.addSeparator()
		
		# 2) Tagging operations
		# - bookmarking
		if folder.bookmarked:
			# XXX: needs dedicated icon
			actBookmarkRemove = menu.addAction(qgui.QIcon(":/images/add_bookmark.png"),
											   "Remove Bookmark...")
			actBookmarkRemove.triggered.connect(self.on_RemoveBookmark)
		else:
			actBookmarkAdd = menu.addAction(qgui.QIcon(":/images/add_bookmark.png"), 
										 "Add Bookmark...")
			actBookmarkAdd.triggered.connect(self.on_AddBookmark)
		
		# - ignoring items
		actIgnore = menu.addAction(qgui.QIcon(":/images/folder_ignore.png"),
									 "Ignore...")
		actIgnore.triggered.connect(self.on_IgnoreSubtree)
							
							
		menu.addSeparator()
		
		# 3) Searching operations - "find similar"
		
		#menu.addSeparator()
		
		# 4) Properties
		actProps = menu.addAction("Properties...")
		
		# show menu -----------------------------------------
		
		res = menu.exec_(self.mapToGlobal(event.pos()))
			
	
	
	# Callback for opening new tab
	def on_OpenNewTab(self):
		folder = self.currentFolder()
		if folder is None:
			return
			
		# HACK: open folder in new tab
		# TODO: the SCOFT bar should be displayed with a reduced context as a result
		win = self.window()
		win.addNewTab(folder.path(fullPath=False, asStr=True))
	
	# Callback for ignoring a subtree
	def on_IgnoreSubtree(self):
		folder = self.currentFolder()
		if folder is None:
			return
			
		# ignore folder and its subtree...
		folder.ignoreTree()
		self.filters.filtersChanged.emit()
	
	# Callback for adding bookmark
	def on_AddBookmark(self):
		folder = self.currentFolder()
		if folder is None:
			return
		
		folder.itemBookmark(True)
		self.filters.filtersChanged.emit() # XXX
		
	# Callback for removing bookmark
	def on_RemoveBookmark(self):
		folder = self.currentFolder()
		if folder is None:
			return
		
		folder.itemBookmark(False)
		self.filters.filtersChanged.emit() # XXX

##########################################
# Folder Pane - The widget to include in the browser

# Folder Pane - Combines tree and overview
class FsFolderPane(qgui.QWidget):	
	# ctor
	# < fs_model: (FsModel)
	# < navmodel: (NavModel)
	# < filters: (FilterModel)
	# < features: ({str:bool}) dict of "feature-name" <-> enabled-status flags
	def __init__(self, fs_model, navmodel, filters, features):
		qgui.QWidget.__init__(self)
		
		# store links to model
		self.fs_model = fs_model
		self.navmodel = navmodel
		self.filters = filters
		
		# settings
		# - cache off the whole dict for convenience, but save off important items as well
		self.features = features
		
		#self.show_tree = features.get('use_tree', True)
		self.use_jumps = features.get('use_jumps', True)
		
		# internal state locks - to prevent un-necessary selection+repaint flushing...
		self._internal_sel_change_lock = False		# when True, on_SelectionChanged() should not fire...
		
		# init widgets and bind events
		self.setup_ui()
		self.bind_events()
		
	# setup widgets
	def setup_ui(self):
		hbox = qgui.QHBoxLayout()
		
		#hbox.setSpacing(1)
		#hbox.setMargin(2)
		
		hbox.setSpacing(0)
		hbox.setMargin(0)
		
		self.setLayout(hbox)
		
		# traditional tree list
		# TODO: the type of pane to display here depends on what has been selected
		self.tree = FsFolderTree(self.fs_model, self.filters, self.features)
		hbox.addWidget(self.tree)
	
	# setup events
	def bind_events(self):
		# hover over items -> highlight in list
		self.tree.setMouseTracking(True) # for highlight tracking to work
		self.tree.itemEntered.connect(self.on_HighlightMove)
		
		# select item
		self.tree.itemSelectionChanged.connect(self.on_SelectionChanged)
		self.tree.itemClicked.connect(self.on_TreeClick)
		
		# filters updating
		self.filters.filtersChanged.connect(self.tree.updateModel)
		self.filters.refreshRankings.connect(self.tree.updateModel)
		
		# location changed
		self.navmodel.locationChanged.connect(self.on_LocationChanged)
	
	# Build models - called only after widgets are built
	def initUI(self):
		#self.viz.build_display()
		self.tree.rebuildModel()
	
	# Reset expand status of items
	# ! This is based on the assumption that the navmodel has been reset already (so current location is changed)
	def resetUI(self):
		if self.tree.logger: self.tree.logger.logging = False
		
		# reset expanded status of items in tree
		for item in self.tree.tree_list:
			parent = item.parent()
			folder = self.tree.folder_from_item(item)
			
			if self.use_jumps:
				# see FsFolderTree.build_tree() for default logic here
				if ( (parent is None) or 
					 (folder.isChainLink() and (parent.parent() is not None)) ):
					# expand me!
					item.setExpanded(True)
				else:
					# collapsed by default
					item.setExpanded(False)
			else:
				# otherwise, only the root is open
				# XXX: in default view, only the first root is open...
				if (parent is None) and (item == self.tree.tree_list[0]):
					item.setExpanded(True)
				else:
					item.setExpanded(False)
					
		if self.tree.logger: self.tree.logger.logging = True
	
	# ===========================================================
	# Tree <-> Viz Interactions
	
	# highlight item under cursor
	def on_HighlightMove(self, item, col):
		try:
			# use the index of the item under the cursor
			index = self.tree.itemIndex(item)
			
			# set index and force redraw to show it
			self.viz.setHighlightIndex(index)
		except:
			pass
			
	# highlight selected subtree
	# ! Gets called from 
	#   - tree.selectionChanged() signal as result of clicks
	#   - on_JumpToItem() as part of summary clicks flushing
	def on_SelectionChanged(self):
		# do NOT do anything if we're still in the middle of making 
		# changes internally... (i.e. on_JumpToItem() is changing state...)
		if self._internal_sel_change_lock:
			return
		
		# user-action on tree resulted in selection change...
		try:
			# find the index for the first selected item
			item = self.tree.selectedItems()[0]
			index = self.tree.itemIndex(item)
			
			# set index and force redraw to show it
			# TODO: convenience func for this needed
			self.viz.selected_index = index
			self.viz.repaint()
		except:
			pass
			
	# Navigate to location on clicking in traditional tree
	def on_TreeClick(self, item):
		# go to this location
		folder = self.tree.folder_from_item(item)
		self.navmodel.setLocation(folder)
	
	# show item at index specified (from clicking on summary)
	def on_JumpToItem(self, index):
		#print "jump to item - %s" % (index)
		
		# don't jump for negative indices (this is used for nothing found in the SCOFT bar)
		if index == -1:
			#print "ignoring negative index: on_JumpToItem(%s)" % (index)
			return
		
		# find item at the relevant index
		item = self.tree.findItemFromIndex(index)
		
		# perform actions with this item
		if item:
			if self.tree.logger: self.tree.logger.logging = False
			
			# Always expand to show this item!
			item.setExpanded(True)
				
			# expand up chain - so that we can see the selected item
			par = item.parent()
			while par is not None:
				par.setExpanded(True)	
				par = par.parent()
			
			# scroll to this item
			#self.tree.scrollToItem(item, qgui.QAbstractItemView.EnsureVisible) # XXX: this doesn't work that well for many children
			self.tree.scrollToItem(item, qgui.QAbstractItemView.PositionAtCenter)
			
			# select just this item
			# NOTE: this step will flush the selection of the summary (via on_SelectionChanged)
			#       so we must set the change locks to prevent unwanted changes here...
			self._internal_sel_change_lock = True
			
			for x in self.tree.selectedItems():
				x.setSelected(False)
			
			item.setSelected(True)
			self._internal_sel_change_lock = False
			
			# go to this location
			folder = self.tree.folder_from_item(item)
			self.navmodel.setLocation(folder)
			
			if self.tree.logger: self.tree.logger.logging = True
		else:
			print "ERROR: item not found - %d" % (index)
			
	# navigation model's location changed
	def on_LocationChanged(self):
		# get folder/location
		folder = self.navmodel.location
		
		# get index of folder, and navigate to it if we aren't already there
		# TODO: how do we know? whether we're there yet?
		
		"""
		# get index of folder, and navigate to it if we aren't already there
		for index, vitem in enumerate(self.viz.model):
			# is this the one we want?
			if vitem.data == folder:
				# are we already there?
				if self.viz.selected_index != index:
					# navigate to this index
					self.on_JumpToItem(index)
					
				# only mark as selected *after* we get there (and have flashed)
				# NOTE: don't do any flash/highlight for root of entire viz, as that's too distracting
				if folder != self.navmodel.root:
					# flash target to show that we've reached it
					if (index != -1):
						self.on_flashTargetTriggered(index)
					
					# mark as selected
					self.viz.selected_index = index
				else:
					# this is the root item...
					if len(self.fs_model.tree) == 1:
						# there's just this one, it shouldn't be highlighted, or else it gets distracting
						self.viz.selected_index = -1
					else:
						# there are more than one, so we do want to show how far it extends
						self.viz.selected_index = index
				
				# redraw to show changes
				# XXX: shouldn't this be just the viz?
				self.repaint()
				
				# now we're there!
				break
		"""


		

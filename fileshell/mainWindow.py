# Main window for file browser
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import qrc_resources_rc
from fileshell.theming import stylesheets
from fileshell.theming import windowTheme

from fileshell import browser
from fileshell.components import statsDialog

from fileshell.model.fsutils import *


# Main Window
class FileShell(qgui.QWidget):
	# Setup =======================================
	
	# ctor
	# < fs_model: (fsdb.FsModel) file system model to use
	# < path: (str) path to starting directory to display on startup
	# < features: ({str:bool}) dict of "feature name" <-> enabled-status flags
	def __init__(self, fs_model, path, features):
		super(FileShell, self).__init__()
		
		# window settings
		self.setWindowIcon(qgui.QIcon(":/images/folder.png"))
		self.setWindowTitle("File Browser")
		
		self.resize(960, 600)
		#self.resize(900, 570)
		
		#windowTheme.enableGlassWindowTheme(self, True)
		
		# init data model
		self.fs_model = fs_model
		self.features = features
		
		# setup additional buttons and bind events
		self.setup_ui()
		self.bind_events()
		
		# show window
		# TODO: show with a progress bar which is visible until tab is added
		self.show()
		
		# setup main contents...
		self.setup_browser(path)
		
	# Setup additional buttons 
	def setup_ui(self):
		# setup main layout manager
		self.main_layout = qgui.QVBoxLayout()
		
		self.main_layout.setSpacing(0)
		self.main_layout.setMargin(0)
		
		self.setLayout(self.main_layout)
		self.setContentsMargins(0, 0, 0, 0)
		
		# menubar
		self.setup_appButton()
		
		
	# "App Button"
	def setup_appButton(self):
		# app menu --------------------------------
		self.appButton = qgui.QPushButton("File")
		self.appButton.setToolTip("Click here for File Browser options and settings")
		
		self.appButton.setObjectName("AppButton")
		self.appButton.setStyleSheet(stylesheets.AppButton)
		#self.appButton.setFixedSize(qcore.QSize(68, 20))
		
		self.setup_appMenu()
		self.appButton.setMenu(self.appMenu)
		
		# menubar ----------------------------------
		hbox = qgui.QHBoxLayout()
		hbox.setSpacing(0)
		hbox.setMargin(0)
		
		self.main_layout.addLayout(hbox)
		
		# app button in top corner
		hbox.addWidget(self.appButton)
		
		# extra space at the end to force enough spacing...
		hbox.addStretch(1)
	
	# Menu for "app button"
	def setup_appMenu(self):
		menu = self.appMenu = qgui.QMenu()
		
		# New window/tab functionalities ------------
		actNewWin = menu.addAction("New Window")
		
		#actPrivateToggle = menu.addAction("Start Private Browsing")
		#actPrivateToggle.setToolTip("Start a new session where FileShell won't save information about where you've been")
		
		# Database Management -----------------------
		menu.addSeparator()
		
		actStats = menu.addAction("Statistics...")
		actStats.setToolTip("Show keys statistics about file system database")
		actStats.triggered.connect(self.on_StatsShow)
		
		actUpdate = menu.addAction("Update Database...")
		actUpdate.setToolTip("Update file system database to in response to major changes made outside of FileShell")
		
		#actManage = menu.addAction("Manage Locations...")
		#actManage.setToolTip("Add/Remove directories from SCOFT bar for easy access")
		
		actReset = menu.addAction("Reset Browser")
		actReset.setToolTip("Reset browser interface to initial state")
		actReset.triggered.connect(self.resetUI)
		
		# General Settings ---------------------------
		menu.addSeparator()
		
		#actTimeFilterToggle = menu.addAction("Time-Based Filtering")
		#actTimeFilterToggle.setToolTip("Fade out items that weren't changed within the specified time period to make it easier to find things")
		#actTimeFilterToggle.setCheckable(True)
		#actTimeFilterToggle.setChecked(True) # XXX: always on for nows
		
		actSettings = menu.addAction("Preferences...")
		
		# About --------------------------------------
		menu.addSeparator()
		
		actAboutQt = menu.addAction("About Qt")
		actAboutQt.triggered.connect(self.on_AboutQt)
		
		actAbout = menu.addAction("About...")
		actAbout.triggered.connect(self.on_AboutApp)
		
		
		actExit = menu.addAction("Exit")
		actExit.triggered.connect(self.on_ExitApp)
		
	# Bind event handling things
	def bind_events(self):
		self.bind_MenubarToggle()
		
	def bind_MenubarToggle(self):
		# HACK: only make menubar visible when F1 key is held down
		actToggleMenubar = qgui.QAction("Toggle Menubar Visible", self)
		actToggleMenubar.setShortcut(qgui.QKeySequence.fromString("Alt+X"))
		
		def toggleMenubar():
			#print "toggleMenubar"
			self.appButton.setVisible(not self.appButton.isVisible())
			
		actToggleMenubar.triggered.connect(toggleMenubar)
		self.addAction(actToggleMenubar)
	
		self.appButton.setVisible(False)
	
	# Event Handling ============================================================
	
	# Setup browser pane
	def setup_browser(self, path):
		with TimedOperation("Setting up browser..."):
			qgui.QApplication.setOverrideCursor(qgui.QCursor(qcore.Qt.WaitCursor))
			self.bpane = browser.BrowserInterface(self.fs_model, path, self.features)
			
			if path:
				name = get_destination_name(path)
			else:
				name = "File Browser"
			
			# add tab
			self.main_layout.addWidget(self.bpane, stretch=1)
			
			# finish off setting up browser
			self.bpane.initUI()
			qgui.QApplication.restoreOverrideCursor()
	
	# Update title when location being viewed changes
	def refreshLocation(self, name):
		# set window title to that of the active tab
		self.setWindowTitle("%s - File Browser" % (name))
		
	# Reset UI to initial state
	def resetUI(self):
		qgui.QApplication.setOverrideCursor(qgui.QCursor(qcore.Qt.WaitCursor))
		
		self.bpane.resetUI()
			
		qgui.QApplication.restoreOverrideCursor()
		
	# Close application
	def on_ExitApp(self):
		print "Closing..."
		# TODO: need to perform cleanup operations!
		self.deleteLater()
		
	# Show "About Qt" Dialog
	def on_AboutQt(self):
		qgui.QMessageBox.aboutQt(self)
		
	# Show "About" dialog
	def on_AboutApp(self):
		aboutText  = "<p><b>SCOFT File Browser</b><br>"
		aboutText += "Version: %s (%s)<br>" % ("3.0", "January 2014")
		aboutText += "Author: Joshua Leung (aligorith@gmail.com)</p>"
		aboutText += "<p>An experimental file browser acting as a test bed for some of my research work "
		aboutText += "aimed at exploring techniques for improving file browsing efficiency, and/or "
		aboutText += "spatial consistency.</p>"
		aboutText += "<p>Icons Taken From: <a href='http://tango.freedesktop.org'>Tango</a> and "
		aboutText += "<a href='http://www.oxygen-icons.org'>Oxygen</a> icon sets</p>"
		
		qgui.QMessageBox.about(self, "About File Browser", aboutText)
		
	# Show "Statistics" dialog
	def on_StatsShow(self):
		# WARNING: sometimes this can be slow!
		qgui.QApplication.setOverrideCursor(qcore.Qt.WaitCursor)
		
		dlg = statsDialog.StatsDialog(self.fs_model, self)
		dlg.show()
		
		qgui.QApplication.restoreOverrideCursor()
		

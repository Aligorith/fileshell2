# File browser panel - Body of each tab
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import stylesheets

from fileshell.components.navbar import Navbar

from fileshell.model.filters import FilterModel
from fileshell.model.navmodel import NavModel

from fileshell.filewidgets.foldertree import FsFolderPane

from fileshell.filewidgets.fileview import StandardFileList

#########################################

# File Browser interface
class BrowserInterface(qgui.QWidget):
	# ctor
	# < fs_model: (FsModel)
	# < (startPath): (str | FolderData)
	# < (features): ({str:bool}) dict of "feature name" <-> "enabled status" mappings 
	# < (private): (bool) log each folder access made - FOR NOW/TESTING, THIS IS TURNED ON TO PREVENT BIAS
	def __init__(self, fs_model, startPath=None, features=None, private=True):
		qgui.QWidget.__init__(self)
		
		# init models
		self.fs_model = fs_model				# file system model (full dataset)
		
		self.navmodel = NavModel(fs_model, startPath, private) # navigation stuff
		self.filters = FilterModel(fs_model)	# model for the filters used
		
		# init settings
		self.features = features
		
		# init ui components
		self.setup_ui()
		
		# connect special cross-component event handlers
		try:
			self.itemView.folderHighlightChanged.connect(self.folderPane.viz.setHighlightIndex)
		except:
			print "FileView -> SCOFT item highlights hookup failed..."
			pass
		
		try:
			self.itemView.postItemOpen.connect(self.folderPane.on_flashTargetTriggered)
		except:
			print "FileView -> FolderPane.SCOFT tag flash trigger failed..."
			pass
		
		# connect models to UI components
		self.navmodel.locationChanged.connect(self.on_LocationChanged)
		
	# Setup UI ============================================================
	
	# setup ui components
	def setup_ui(self):
		# layout container
		vbox = qgui.QVBoxLayout()
		
		vbox.setSpacing(0)
		vbox.setMargin(0)
		
		self.setLayout(vbox)
		self.setContentsMargins(0, 0, 0, 0)
		
		# navigation bar ..........................
		self.setup_navbar()
		vbox.addWidget(self.navbar, stretch=0)
		
		# views ....................................
		
		# hsplitter contains the two folder/file views to allow resizing
		self.splitter = qgui.QSplitter()
		self.splitter.setHandleWidth(2)
		vbox.addWidget(self.splitter, stretch=1)
		
		# tree views
		# TODO: this needs review...
		self.folderPane = FsFolderPane(self.fs_model, self.navmodel, self.filters, self.features)
		
		self.splitter.addWidget(self.folderPane)
		self.splitter.setStretchFactor(0, 0)
		
		# icon grid
		self.setup_itemView()
		self.splitter.addWidget(self.itemsHost)
		self.splitter.setStretchFactor(1, 20)
		
	# navigation bar
	def setup_navbar(self):
		# setup widget
		self.navbar = Navbar()
		
		self.navbar.attachFilterModel(self.filters)
		self.navbar.attachNavigationModel(self.navmodel)
	
	# main content view
	def setup_itemView(self):
		# layout
		vbox = qgui.QVBoxLayout()
		vbox.setSpacing(0)
		vbox.setMargin(0)
		
		self.itemsHost = qgui.QWidget()
		self.itemsHost.setContentsMargins(1,0,1,0)
		self.itemsHost.setLayout(vbox)
		
		# item view
		self.itemView = StandardFileList(self.fs_model, self.navmodel, self.filters, self.features)
		vbox.addWidget(self.itemView, stretch=1)
		
		# statusbar
		#self.statusbar = qgui.QStatusBar()
		#vbox.addWidget(self.statusbar, stretch=0)
		
		# TODO: area for a cross-directory clipboard...
	
	# Methods ==========================================
	
	# Finish initialising browser
	def initUI(self):
		# build shared cache models in filters
		self.filters.updateCaches()
		
		# build models in various views
		self.folderPane.initUI()
		
		# flush the navigation model so that everything is valid
		self.navmodel.locationChanged.emit()
		
	# Reset UI to initial state
	def resetUI(self):
		# reset filemodel
		self.fs_model.reset()
		
		# set navmodel to initial location
		self.navmodel.reset()
		
		# reset folder tree expand status
		self.folderPane.resetUI()
		
		# reset filters
		self.filters.reset()
	
	# Event Callbacks ==================================
	
	# update parent on location change
	def on_LocationChanged(self):
		# grab parent tab-host
		owner = self.window()
		
		# refresh current tab's name
		name = self.navmodel.location.name
		owner.refreshLocation(name)

#########################################

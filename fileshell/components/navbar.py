# Navigation bar
# Author: Joshua Leung

import PyQt4
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

from fileshell.theming import stylesheets

from fileshell.components.addressbox import AddressBar
from fileshell.components.searchWidget import SearchBox

###########################################
# Navigation Buttons

# Generate context menu showing history for navigation
def historybutton_show_list(but, event, navmodel):
	# only show if there is history
	if not (navmodel.canGoBack() or navmodel.canGoForward()):
		return
	
	# create the menu --------------------------------
	menu = qgui.QMenu(but)
	
	# map from item to function + steps pair
	act_map = {}
	
	# add future list first - add in defined order, as it is reversed, meaning newest event is first
	for i, item in enumerate(navmodel.later):
		act = menu.addAction(item.name)
		
		# i starts from 0, but step needs to be maximum (last item - i.e. first forward step) = 1 step)
		steps = len(navmodel.later) - i
		act_map[act] = (navmodel.goForward, steps) 
		
	# current location
	act = menu.addAction(qgui.QIcon.fromTheme("folder", qgui.QIcon(":/images/folder.png")),
                         navmodel.location.name)
	menu.setDefaultAction(act)
	
	# add in reverse order so that newest is closest
	for i, item in enumerate(reversed(navmodel.previous)):
		act = menu.addAction(item.name)
		
		# i starts from 0, so need to add 1 to take a step
		steps = i + 1
		act_map[act] = (navmodel.goBack, steps)
	
	# show menu -----------------------------------------
	
	# get item clicked
	res = menu.exec_(but.mapToGlobal(event.pos()))
	
	# execute action
	if res in act_map:
		fn, steps = act_map[res]
		fn(steps)


# Back Button
class BackButton(qgui.QToolButton):
	# ctor
	def __init__(self):
		qgui.QToolButton.__init__(self)
		
		# standard theming and appearance stuff
		self.setObjectName("backBut")
		self.setIcon(qgui.QIcon(":/images/arrow-back.png"))
		self.setShortcut(qgui.QKeySequence.Back)
		self.setToolTip("<b>Back</b><br />Right-click to show history")
		
		# fixed size - should be nice and big to click on
		self.setFixedSize(36, 32)
		
	# hook up navigation model
	def attachNavigationModel(self, navmodel):
		self.navmodel = navmodel
		
		# main action
		self.clicked.connect(self.navmodel.goBack)
		
		# enabled status
		self.navmodel.locationChanged.connect(self.update_status)
		
	# update enabled status in response to current location changing
	def update_status(self):
		self.setEnabled(self.navmodel.canGoBack())
		
	# context menu - for navigating to past items
	def contextMenuEvent(self, event):
		# show and handle the menu
		historybutton_show_list(self, event, self.navmodel)

# Forward Button
class ForwardButton(qgui.QToolButton):
	# ctor
	def __init__(self):
		qgui.QToolButton.__init__(self)
		
		# standard theming and appearance stuff
		self.setObjectName("forwardBut")
		self.setIcon(qgui.QIcon(":/images/arrow-forward.png"))
		self.setShortcut(qgui.QKeySequence.Forward)
		self.setToolTip("<b>Forward</b><br />Right-click to show history")
		
		# fixed size - slightly smaller than back
		self.setFixedSize(32, 32)
		
	# hook up navigation model
	def attachNavigationModel(self, navmodel):
		self.navmodel = navmodel
		
		# main action
		self.clicked.connect(self.navmodel.goForward)
		
		# enabled status
		self.navmodel.locationChanged.connect(self.update_status)
		
	# update enabled status in response to current location changing
	def update_status(self):
		self.setEnabled(self.navmodel.canGoForward())
		
	# context menu - for navigating to past items
	def contextMenuEvent(self, event):
		# show and handle the menu
		historybutton_show_list(self, event, self.navmodel)
		
###########################################

# Navigation toolbar
class Navbar(qgui.QWidget):
	# ctor
	def __init__(self):
		qgui.QWidget.__init__(self)
		
		# styling
		self.setObjectName("navbar")
		self.setStyleSheet(stylesheets.Navbar)
		
		# navigation model
		self.navmodel = None
		
		# setup UI components
		self.setup_ui()
		
	# Setup UI ============================
	
	# setup components
	def setup_ui(self):
		# grid layout for utmost control
		gbox = qgui.QGridLayout()
		
		gbox.setContentsMargins(4, 4, 4, 4)
		gbox.setHorizontalSpacing(6)
		gbox.setVerticalSpacing(2)
		
		self.setLayout(gbox)
		
		# setup navigation buttons
		self.setup_navbuts()
		
		# setup addressbar
		self.setup_addressbar()
		
		# setup searchbox
		self.setup_searchbox()
		
	# setup navigation buttons
	def setup_navbuts(self):
		# [<|>]  hbox group --------------------------------------- 
		hbox = qgui.QHBoxLayout()
		hbox.setSpacing(0)
		self.layout().addLayout(hbox, 0, 0)
		
		# back button
		self.backButton = BackButton()	
		hbox.addWidget(self.backButton)
		
		# forward button
		self.forwardButton = ForwardButton()
		hbox.addWidget(self.forwardButton)
		
		# [^] up-button group -------------------------------------
		# up button
		self.upButton = qgui.QToolButton()
		self.upButton.setObjectName("upButton")
		self.upButton.setIcon(qgui.QIcon(":/images/arrow-up.png"))
		self.upButton.setToolTip("Jump up to parent folder")
		
		self.upButton.setFixedSize(24, 24)
		
		self.layout().addWidget(self.upButton, 0, 1)
		
	# setup addressbar
	def setup_addressbar(self):
		self.addressBar = AddressBar()
		
		self.layout().addWidget(self.addressBar, 0, 2)
		self.layout().setColumnStretch(2, 5)
		
	# setup searchbox
	def setup_searchbox(self):	
		self.searchBox = SearchBox()
		
		self.layout().addWidget(self.searchBox, 0, 3)
		self.layout().setColumnStretch(3, 2)
		
	# Custom Painting ====================================
	
	# draw a gradient rect
	def paintEvent(self, event):
		p = qgui.QPainter()
		p.begin(self)
		
		# get extents
		w = self.width()
		h = self.height()
		
		# gradient fill
		fill = qgui.QLinearGradient(0.0, 0.0, 0.0, h)
		fill.setColorAt(0.0, qgui.QColor(88, 145, 221))
		fill.setColorAt(0.4, qgui.QColor(62, 119, 170))
		fill.setColorAt(0.5, qgui.QColor(61, 118, 169))
		fill.setColorAt(0.6, qgui.QColor(51, 98, 140))
		fill.setColorAt(1.0, qgui.QColor(62, 119, 170))
		p.setBrush(fill)
		
		pen = qgui.QPen(qgui.QColor(99, 158, 230), 2)
		p.setPen(pen)
		
		p.drawRect(0, 0, w-1, h-1)
		
		# dark border
		#pen = qgui.QPen(qgui.QColor(60, 60, 60), 1)
		pen = qgui.QPen(qgui.QColor(200, 200, 200), 1)
		p.setPen(pen)
		p.setBrush(qcore.Qt.NoBrush)
		
		p.drawRect(0, 0, w-1, h-1)
		
		p.end()
		
	# Model Binding =======================================
	
	# Attach filter model
	# < filters: (FilterModel)
	def attachFilterModel(self, filters):
		# XXX: ideally we'd get live-search, although that may be too slow, in which case this needs to be disabled
		self.searchBox.textChanged.connect(filters.update_searchText)
		
	# Attach navigation model
	def attachNavigationModel(self, navi):
		# hook up havigation model
		self.navmodel = navi
		
		# back/forward buttons
		self.backButton.attachNavigationModel(navi)
		self.forwardButton.attachNavigationModel(navi)
		
		# up button
		self.upButton.clicked.connect(navi.goUp)
		self.navmodel.locationChanged.connect(self.on_location_changed)
		
		# addressbar
		self.addressBar.attachNavigationModel(navi)
		
	# Navigation Methods =====================================
	
	# Update status of UI in response to location changing
	def on_location_changed(self):
		# convenience define
		navi = self.navmodel
		
		# update button enabled statuses
		self.upButton.setEnabled(navi.canGoUp())
		
###########################################

# File System Statistics Dialog
# 
# For showing statistics about the complexity of the snapshot,
# as well as other metadata which may be useful for analysis.
#
# Author: Joshua Leung

import sys

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import os
from math import *
import time

import PyQt4 as pqt
from PyQt4 import QtCore as qcore
from PyQt4 import QtGui as qgui

######################################################
# Stats Dialog

class StatsDialog(qgui.QDialog):
	# ctor
	def __init__(self, fs_model, parent=None):
		qgui.QDialog.__init__(self, parent, qcore.Qt.WindowTitleHint | qcore.Qt.WindowSystemMenuHint | qcore.Qt.Tool)
		
		# store file system model for later usage
		self.fs_model = fs_model
		self.compute_stats()
		
		# dialog settings
		self.setWindowTitle("File System Statistics")
		
		# setup UI layout
		self.setup_ui()
		
	# UI Setup =================================
	
	# setup layout
	def setup_ui(self):
		show_recents = False
		
		if show_recents:
			# stats + recent items
			hbox = qgui.QHBoxLayout()
			self.setLayout(hbox)
			
			# LHS
			statsPane = qgui.QWidget()
			hbox.addWidget(statsPane)
			self.setup_stats_pane(statsPane)
			
			# RHS
			recentPane = qgui.QWidget()
			hbox.addWidget(recentPane)
			self.setup_recents_pane(recentPane)
		else:
			# just simple stats pane
			self.setup_stats_pane(self)
	
	# statistics pane
	def setup_stats_pane(self, owner):
		form = qgui.QFormLayout()
		owner.setLayout(form)
		
		# size of database
		form.addRow(qgui.QLabel("<b>Total Number of Items</b>"))
		form.addRow("Folders:", qgui.QLabel(self.stats['num_folders']))
		form.addRow("Files:",   qgui.QLabel(self.stats['num_files']))
		
		form.addItem(qgui.QSpacerItem(0, 10))
		
		# shape parameters
		form.addRow(qgui.QLabel("<b>Shape Parameters</b>"))
		form.addRow("Max Depth:", qgui.QLabel(self.stats['max_depth']))
		form.addRow("Density:", qgui.QLabel("%s fpp (@ 500px)" % self.stats['density']))
		
		form.addItem(qgui.QSpacerItem(0, 10))
		
		# time parameters
		form.addRow(qgui.QLabel("<b>Time Extents</b>"))
		form.addRow("Min Time:", qgui.QLabel(self.stats['time_min']))
		form.addRow("Max Time:", qgui.QLabel(self.stats['time_max']))
		
		form.addItem(qgui.QSpacerItem(0, 10))
		
		# database info
		form.addRow("Is Snapshot:", qgui.QLabel(str(self.fs_model.foreign)))
		
	# Recency pane - for debugging what's been most recently touched
	def setup_recents_pane(self, owner):
		layout = qgui.QVBoxLayout()
		owner.setLayout(layout)
		
		layout.addWidget(qgui.QLabel("<b>Recently visited items (one per folder)</b>"))
		
		# list view
		rlist = qgui.QListWidget()
		rlist.resize(rlist.width() * 2, rlist.height())
		layout.addWidget(rlist)
		
		# query recent items
		recent_items = self.fs_model.findRecentItems(as_folders=False, one_per_folder=True, limit=20)
		
		# populate list
		for item in recent_items:
			#itemName = "<b>%s</b>   - %s" % (item.name, item.path(asStr=True))
			
			listItem = qgui.QListWidgetItem(item.path(asStr=True))
			listItem.setData(qcore.Qt.ToolTipRole, self.time_formatter(item.mtime()))
			
			rlist.addItem(listItem)
		
		
	# Stats Computation ========================
		
	# compute stats of fs database
	def compute_stats(self):
		# <str, str>
		self.stats = {}
		
		# item counts 
		self.stats['num_folders'] = str(len(self.fs_model.folders))
		self.stats['num_files']   = str(len(self.fs_model.files))
		
		# time extents
		tmodel = self.fs_model.time_model
		self.stats['time_min'] = self.time_formatter(tmodel.tmin) 
		self.stats['time_max'] = self.time_formatter(tmodel.tmax)
		
		# maximum depth
		# XXX: delay computation of these ones...
		self.stats['max_depth'] = str(self.fs_model.getMaxDepth())
		
		# density - based on standard height (500px)
		self.stats['density'] = str(len(self.fs_model.folders) / 500.0)
		
		# XXX: other more complicated stats to come later, as support for this is added in model
		
	# time formatting helper
	# < time: (float)
	# > returns: (str)
	def time_formatter(self, t):
		ts = time.localtime(t)
		return time.strftime("%d %b %Y   %I:%M %p", ts) 

######################################################

# Decorative platform-specific theming hacks to get the desired visual
# appearance on different platforms. In particular, this enables the
# "glass" titlebar look for applications
#
# Based on QtWin class from "blurbehind" demo:
# http://labs.qt.nokia.com/2009/09/15/using-blur-behind-on-windows/
#
# For more info, check out the following resources: 
# * Windows-Style:
# 	http://labs.qt.nokia.com/2009/09/15/using-blur-behind-on-windows/
# 	http://www.hardcoded.net/devlogs/20120615.htm
#
# * Ctypes Hardcoded:
#	http://stackoverflow.com/questions/453069/qt-erase-background-windows-aero-glass

import platform

import PyQt4
from PyQt4 import QtGui as qgui
from PyQt4 import QtCore as qcore

##########################################
# Windows Support

# enable glass theming on windows
def enable_windows_glass(win, overrideTitle):
	from ctypes import windll, c_int, byref
	
	# widget settings
	win.setAttribute(qcore.Qt.WA_NoSystemBackground)
	win.setAttribute(qcore.Qt.WA_TranslucentBackground)
	
	# windows api calls
	windll.dwmapi.DwmExtendFrameIntoClientArea(c_int(win.winId()), byref(c_int(-1)))

##########################################
# Linux Support

def enable_linux_glass(win):
	#win.setAttribute(qcore.Qt.WA_NoSystemBackground)
	#win.setAttribute(qcore.Qt.WA_TranslucentBackground)
	
	# other hacks from BlurBehind: "workaround Oxygen filling the background"
	pal = win.palette()
	bg = pal.window().color()
	bg.setAlpha(180);
	pal.setColor(qgui.QPalette.Window, bg)
	win.setPalette(pal)
	
	win.ensurePolished()
	win.setAttribute(qcore.Qt.WA_StyledBackground, False);

##########################################
# Public API

# Externally-visible API method to enable window theming
# TODO: support drawing into titlearea too?
def enableGlassWindowTheme(win, overrideTitle=False, ):
	osName = platform.system()
	if osName == 'Windows':
		# only from Vista onwards
		if int(platform.version().split('.')[0]) >= 6:
			enable_windows_glass(win, overrideTitle)
			return True
		else:
			return False
	elif osName == 'Linux':
		enable_linux_glass(win)
	else:
		# not supported
		return False

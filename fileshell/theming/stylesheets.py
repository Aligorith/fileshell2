# Stylesheets for FileShell

# Stylesheet for main window tabs ------------------------------------------------
# NOTE: expects the tabs to have Qt name property, "winTabHost"

# Taken directly from blurbehind
def _MainTabs(): pass

MainTabs = """\
 QTabWidget#winTabHost::tab-bar {
	top: 4px;
	border: 0px;
 }
 
 QTabBar#winTabHost::tab { 
	border-image: url(":/images/tab.png") ;
	border-width:4px;
	padding-left:3px;
	min-width:36ex;
	color: black;
 }
 
 QTabBar#winTabHost::tab:!selected {
	margin-top: 3px;
	margin-bottom: 5px;
 }
 
 QTabBar#winTabHost::tab:selected {
	border-image: url(":/images/tab-selected.png");
    margin-bottom: 3px;
 }

 QTabBar#winTabHost::tab:last {  
	border-image: url(":/images/tab-last.png");
 }

 QTabBar#winTabHost::tab:selected:only-one {
	border-image: url(":/images/tab-last-selected.png");
    margin-bottom: 3px;
 }
 
 QTabBar#winTabHost::tab:selected:last {
	border-image: url(":/images/tab-last-selected.png");
    margin-bottom: 3px;
 }
 
 QTabBar#winTabHost::tab:!selected:last:hover { 
	border-image: url(":/images/tab-last-hover.png");
 }
 QTabBar#winTabHost::tab:hover:!selected { 
	border-image: url(":/images/tab-hover.png"); 
 }
 
 /* Body of tab widget */
 QTabWidget#winTabHost::pane {
	border-image: url(":/images/tabframe.png");
	border-width:4;
/*	border-top: 8px;*/
 }
 
 /* Close Button */
 QTabBar#winTabHost::close-button { 
	image: url(":/images/closebutton.png"); 
 }
 QTabBar#winTabHost::close-button:hover:!pressed { 
	image: url(":/images/closebutton_hover.png");
 }
 QTabBar#winTabHost::close-button:pressed {
	image: url(":/images/closebutton_pressed.png");
 }
 
 /* Add Tab */
 QToolButton#AddTab {
	border-image: url(":/images/newtab.png"); 
	border: 2px;
	border-right: 8px;
 }
 QToolButton#AddTab:hover {
	border-image: url(":/images/newtab_hover.png");
 }
 QToolButton#AddTab:pressed {
	border-image: url(":/images/newtab_pressed.png");
 }

"""

AppButton = """\
 QPushButton#AppButton {
	background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #3871FF, stop: 0.1 #3871BB,
								 stop: 0.4 #1C5588, stop: 0.6 #11406A, 
								 stop: 1.0 #1C5599);
	border: 1px solid #000055;
	border-radius: 2px;
	
	color: white;
	font: bold;
	
	padding: 3px;
	padding-left: 25px;
	padding-right: 25px;
	
	margin-left: 1px;
	margin-right: 2px;
	margin-top: 0px;
	margin-bottom: 0px;
 }
 QPushButton#AppButton:hover {
	background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #5891EE, stop: 0.3 #3E77BB,
                                 stop: 0.5 #33629D, stop: 1.0 #3E77BB);
	
	border: 1px solid #111155;
	border-radius: 3px;
 }
 QPushButton#AppButton:pressed {
	background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #165099, stop: 0.5 #0A3366,
                                 stop: 0.7 #112049, stop: 1.0 #1A3377);
	
	border: 2px solid #111155;
	border-radius: 3px;
 }
 
 QPushButton#AppButton::menu-indicator {
	width: 10px;
	height: 10px;
	
	subcontrol-origin: padding;
	subcontrol-position: bottom right;
 }
 QPushButton#AppButton::menu-indicator:pressed, 
 QPushButton#AppButton::menu-indicator:open 
 {
	color: black;
	
	width: 5px;
	height: 5px;
 }

"""

# Stylesheet for toolbars ------------------------------------------------

def _Toolbar(): pass

Toolbars = """\
 QToolBar {
     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #5891DD, stop: 0.4 #3E77AA,
                                 stop: 0.6 #33628c, stop: 1.0 #3E77AA);
                                 
     spacing: 4px; /* spacing between items in the tool bar */
     padding: 10px;
     border: 1px solid #33628c;
     
     margin-bottom: 1px;
 }
 
 QToolButton {
     color: #FFFFFF;
 }
"""

# Stylesheet for navbar ------------------------------------------------------

def _Navbar(): pass

Navbar = """\
 /* Base widget */
 QWidget#navbar {
     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #5891DD, stop: 0.4 #3E77AA,
                                 stop: 0.6 #33628c, stop: 1.0 #3E77AA);
                                 
     spacing: 4px; /* spacing between items in the tool bar */
     padding: 10px;
     border: 1px solid #33628c;
     
	 margin: 0px;
     margin-bottom: 1px;
 }
 
 /* Standard button */
 QToolButton {
	border-image: url(":/images/button.png");
	margin: 0px;
 }
 QToolButton:hover {
	border-image: url(":/images/button-hover.png");
 }
 QToolButton:pressed {
	border-image: url(":/images/button-pressed.png");
 }
 
 /* Back Button */
 QToolButton#backBut {
	border-image: url(":/images/button-left.png");
 }
 QToolButton#backBut:hover {
	border-image: url(":/images/button-left-hover.png");
 }
 QToolButton#backBut:pressed {
	border-image: url(":/images/button-left-pressed.png");
 }
 
 /* Forward Button */
 QToolButton#forwardBut {
	border-image: url(":/images/button-right.png");
 }
 QToolButton#forwardBut:hover {
	border-image: url(":/images/button-right-hover.png");
 }
 QToolButton#forwardBut:pressed {
	border-image: url(":/images/button-right-pressed.png");
 }
 
 /* All textboxes */
 QLineEdit {
	border-radius: 1px;
 }
 
 /* Textbox - Highlight on select */
 QLineEdit:focus {
	border: 2px inset palette(highlight);
 }
"""

# Stylesheets for FileView ----------------------------------------------

def _FileView(): pass

LeftRightViewButs = """\
 /* Prev Button */
 QPushButton#PrevSiblingBut {
	border-image: url(":/images/side-button-inactive.png");
	
	padding: 10px;
	margin: 0px;
 }
 QPushButton#PrevSiblingBut:hover {
	border-image: url(":/images/side-button.png");
 }
 
 /* Next Button */
 QPushButton#NextSiblingBut {
	border-image: url(":/images/side-button-inactive.png");
	
	padding: 10px;
	margin: 0px;
 }
 QPushButton#NextSiblingBut:hover {
	border-image: url(":/images/side-button.png");
 }
"""

# Stylesheet for FloatingLens Tree -------------------------------------

def _FloatingLensTree(): pass

FloatingLensTree = """\
	/* No expanders */
	QTreeView::branch {
		background: palette(base);
		border-image: none;
		image: none;
	}

	QTreeView::branch:has-siblings:!adjoins-item {
		border-image: none;
		image: none;
	}

	QTreeView::branch:has-siblings:adjoins-item {
		border-image: none;
		image: none;
	}

	QTreeView::branch:!has-children:!has-siblings:adjoins-item {
		border-image: none;
		image: none;
	}

	QTreeView::branch:closed:has-children:has-siblings {
		border-image: none;
		image: none;
	}

	QTreeView::branch:has-children:!has-siblings:closed {
		border-image: none;
		image: none;
	}

	QTreeView::branch:open:has-children:has-siblings {
		border-image: none;
		image: none;
	}

	QTreeView::branch:open:has-children:!has-siblings {
		border-image: none;
		image: none;
	}
	
	/* Give items a bit more padding */
	QTreeView::item {
		padding: 1.3px;
	}
"""

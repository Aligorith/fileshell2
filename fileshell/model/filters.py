# Object to hold all the filtering information,
# and to notify everyone else to update themselves#
#
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import PyQt4
from PyQt4 import QtCore as qcore

import os
from difflib import SequenceMatcher as DSM

from fileshell.model import fsdb
from fileshell.model.fsutils import *

######################################################
# Filter Model

class FilterModel(qcore.QObject):
	# Extension Filters ==============================
	
	# List of "bad" extensions (usually to be ignored)
	# FIXME: these should be user editable...
	BAD_EXTENSIONS = set([
		# generic
		".tmp",
		".swp",
		
		# python specific
		".pyc",
		
		# code specific 
		".manifest",
		".dll",
		
		".pdb",
		".dblite",
		
		".o",
		".a",
		".lib",
		
		".class",
		
		# latex
		".synctex.gz",
		".aux",	
		".toc",
		".out",
		".log",
		".bbl",
		".blg"
	])
	
	# Slots ==========================================
	
	# Connect update_filters() methods to this to refresh on demand
	filtersChanged = qcore.pyqtSignal()
	
	# Connect lightweight refresh_rankings() methods to refresh just the changed
	# items on demand (after navigation steps)
	#
	# < index: (Folder Display Index) index to folder that changed
	refreshRankings = qcore.pyqtSignal(int)
	
	# Setup ==========================================
	
	# ctor
	def __init__(self, fs_model):
		qcore.QObject.__init__(self)
		
		# hang on to file system model for later
		self.fs_model = fs_model
		
		# Caches ----------------------------------
		# These basically cache shared filtering state info between representations
		
		# map of {FolderData : isValid()} statuses to be shared between foldertree and file view
		# as fileview cannot "see" any levels above and below to see if it any of its items should
		# be enabled
		self.folderOkOverview = {}
		
		# map of {FileData : isValid()} statuses to be cached as a result of the filtering updates
		# so that all widgets can use the same copy of the filtering info
		self.fileOkOverview = {}
		
		# map of {FolderData : index-int} - mapping from folder objects to the indices they have in
		# various display models (in particular, relative to the overall tree hierarchy shown)
		self.folderDisplayIndices = {}
		
		# map of relevance-strength for each folder item in scoft
		# {FolderData : float [0, 1]}
		self.folderColorHints = {}
		
		# set of "important" items (FolderData)
		self.importantFolders = set()
		
		# Filter Settings ----------------------------------
		
		# search text
		self.search_text = ""          # search string to be matched
		
		self.search_approximate = True # (bool) if enabled, there is no built-in tolerance for typos enabled...
		self.similarity_thresh = 0.75  # as recommended by difflib, for most "normal" text
		
		# timer for delaying when we send "filters changed" when search text changes
		# as doing a full flush takes ages...
		self.search_flush_delay_timer = qcore.QTimer()
		self.search_flush_delay_timer.setInterval(120)     # should be short enough to be a "break" in typing
		self.search_flush_delay_timer.setSingleShot(True) # assume that by resetting the timer, we can call it again, but it won't carry on while we aren't done
		self.search_flush_delay_timer.timeout.connect(self.filtersChanged)
		self.search_flush_delay_timer.stop()
		
		# time range
		self.use_time_filter = False
		
		tmodel = fs_model.time_model
		
		self.tmin = tmodel.tmin
		self.tmax = tmodel.tmax
		
		
		# hook up own "perform all caching updates" method - for updating filters
		self.filtersChanged.connect(self.updateCaches)
		
	# Update Methods ================================
	
	# Update cached values - in response to filter change
	def updateCaches(self):
		# traverse tree in order, propagating values up the tree as needed...
		def tree_update_cb(folder, parent, depth, data):
			# update all children
			childOk = False
			
			for child in folder.files:
				ok = self.itemCanShow(child)
				
				self.fileOkOverview[child] = ok
				childOk |= ok
			
			# update folder itself
			ok = self.itemCanShow(folder)
			folderOk = ok or childOk
			
			self.folderOkOverview[folder] = folderOk
			
			# propagate ok status up tree
			if folderOk:
				par = parent
				while (par is not None) and (not self.folderOkOverview[par]):
					self.folderOkOverview[par] = True
					
					if par.parent:
						par = par.parent()
					else:
						par = None
						
			# update display index for folder
			self.folderDisplayIndices[folder] = len(self.folderDisplayIndices)
					
		# update caches...
		# TODO: don't update if nothing has changed locally...
		with TimedOperation("Updating Filter Caches..."):
			# reset "display indices" cache - it's easy to just rebuild it everytime...
			self.folderDisplayIndices = {}
			
			# perform traversal...
			for root in self.fs_model.tree:
				root.traverseFolders(tree_update_cb)
	
	# ----------------------------------------------
	
	# Reset to default settings
	def reset(self):
		# clear search string
		self.search_text = ""
		
		# reset time model
		tmodel = self.fs_model.time_model
		self.tmin = tmodel.tmin
		self.tmax = tmodel.tmax
		
		# force an update
		# XXX: is update really needed?
		self.filtersChanged.emit()
	
	# ----------------------------------------------
	
	# Time range changed
	def update_timeRange(self, tmin, tmax):
		# save off the new values
		changed = False
		
		if self.tmin != tmin:
			self.tmin = tmin
			changed = True
			
		if self.tmax != tmax:
			self.tmax = tmax
			changed = True
		
		# force an update
		if changed:
			self.filtersChanged.emit()
		
	# Search text changed
	def update_searchText(self, txt):
		# save and update if different
		if self.search_text != txt:
			self.search_text = txt
			
			# (re)start update timer for triggering filtersChanged()
			# - don't flush immediately, otherwise will cause lag
			if self.search_flush_delay_timer.isActive():
				self.search_flush_delay_timer.stop()
				
			self.search_flush_delay_timer.start()
			#self.filtersChanged.emit()
			
	
	# Filtering Methods ==============================
	
	# Check if a given item satisfies the filter conditions
	# < item: (FsItem)
	# > returns: (bool) True if item can be shown in filtered list
	def itemCanShow(self, item):
		# time-range filtering
		if not self.use_time_filter:
			timeOk = True
		else:
			timeOk = self.tmin <= item.mtime() <= self.tmax
		
		# search text
		if not self.search_text:
			searchOk = True
		elif self.search_approximate:
			# partial name match only - much faster
			searchOk = self.search_text in item.name
		else:
			# full-blown search - this is SLOW
			# 1) partial name match
			# 2) similarity of text (for fuzzy matching of misspelled stuff)
			# 3) regrex/wildcards... (TODO)
			searchOk = ((self.search_text in item.name) or
			            (DSM(None, self.search_text, item.name).ratio()	>= self.similarity_thresh) )
		
		# filetype
		if isinstance(item, fsdb.FileData):
			# check filename extension:
			# 1) it shouldn't end in '~', otherwise it is a vim backup
			# 2) it starts with a "."
			# 3) it shouldn't be one of the "bad" extensions (first extension)
			# 4) it shouldn't be one of the "bad" extensions (second extension) - "abc.synctex.gz"
			fname = item.name
			
			BAD = FilterModel.BAD_EXTENSIONS
			base1, ext1 = os.path.splitext(fname)
			ext2 =        os.path.splitext(base1)[1] + ext1
			
			# perform check...
			typeOk = ((fname[0] != '.') and (fname[-1] != '~') and
			          (ext1 not in BAD) and (ext2 not in BAD) )
		else:
			# folder...
			typeOk = True
		
		# only ok if both ok
		return timeOk and searchOk and typeOk

######################################################

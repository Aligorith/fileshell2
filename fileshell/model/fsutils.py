# Shared utilities for handling file names
# Author: Joshua Leung

import os
import time
import traceback

################################################

# Get folder/file name
def get_destination_name(path):
	if os.path.isfile(path):
		# make path absolute, then take the filename only
		name = os.path.basename(path)
	else:
		# strip trailing slashes, then split off last chunk
		sep = '/' if path.startswith('/') else '\\'
		path = path.rstrip(sep)
		name = path.split(sep)[-1]
		
	return name

# Change extension of file
def change_extension(path, ext):
	return os.path.splitext(path)[0] + ext
	
################################################
	
# Utility to measure the amount of time taken from a step
#
# Usage:
# 	with TimedOperation("<Op-Description>"):
#		<...operation-goes-here...>
#
# This will report the amount of time taken for the operation in seconds
class TimedOperation:
	# ctor - creation of object
	def __init__(self, title):
		self.title = title
	
	# enter - start timing
	def __enter__(self):
		# print header
		print self.title
		
		# grab timestamp
		self.time = time.time()
		
	# exit - stop timing
	def __exit__(self, type, value, tb):
		if (type is None) and (value is None) and (tb is None):
			# if exited nicely, print timing
			print "... Took %.2f seconds" % (time.time() - self.time)
		else:
			# otherwise, we had an error
			print traceback.format_exc()
	
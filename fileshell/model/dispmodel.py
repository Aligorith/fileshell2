# Display model - An intermediate data representation of folders
# with additional information useful for displaying these in a
# spatially stable file system visualisation.
#
# Author: Joshua Leung
# NOTE: Based on old implementation in fishprints.py

################################################################
# FsTreeListItem - An Item in the Tree

# Item in the treelist
class FsTreeListItem(object):
	__slots__ = ('name', 'depth', 'data',
				 'length', 'color', 
				 'weight', 'weightSum', 'ybounds',
				 'tMult', 'important',
				 'parItem', 'childItems')
	
	# ctor
	def __init__(self, name, depth, data, parItem):
		# id info
		self.name = name
		self.depth = depth
		
		# data being represented
		# (FolderData)
		self.data = data
		
		
		# "parent" list item
		# (FsTreeListItem)
		self.parItem = parItem
		
		# "child" list items
		# ([FsTreeListItem]) - each child adds itself to its parent
		self.childItems = []
		
		# add self to parent list item's child list
		if self.parItem:
			self.parItem.childItems.append(self) 
		
		
		# fisheye distortion weights
		self.weight = 1.0				# log/damped weight of own influence - initialise with a dummy first
		self.weightSum = 0.0			# sum of weights of children
		
		self.ybounds = None             # (ymin:float, ymax:float)
		
		# visualisation data
		self.length = len(self.name) 	# length of line - starts from length of name
		self.color = [0, 0, 0] 		 	# all use black as default (colors are 0-255 range for PyQt)
		
		self.tMult = 1.0				# multiplying factor - used to emphasize important items (by time)
		self.important = False			# is item an important target (i.e. appears in consecutive bins)
		
	# Importance Handling (Weights, Colours, etc.) -----------------------------------
	
	# Update weight(s) of node
	# ! Assume that when a node is visited, none of its subtree has been seen yet
	# ! Clears the sum total of subtree weights first, to prevent accumulation errors
	# < filters: (FilterModel) filters used to determine what is in range (and not)
	# < (distort_space): (bool) weights for targets varies...
	# < (propagate): (bool) propagate new weight up tree
	def update_ranking(self, filters, distort_space=True, propagate=True):
		# reset sum of subtree weights - assumes that all of those are yet to come
		self.weightSum = 0.0
		
		# determine weight + importance...
		if self.data.ignore:
			# item has been tagged as a "bad" node - ignore and try our best to hide...
			self.weight = 0.0
			
			self.tMult = 0.0
			self.important = False
		elif self.data.bookmarked:
			# user has explicitly specified item as being interesting
			self.weight = 1.0 # this makes it one of the bigger items
			
			# although item is generally important, for search to work, 
			# importance must still follow filters...
			self.important = filters.folderOkOverview[self.data]
		else:
			# compute importance + weight of node normally
			if distort_space:
				#self.important, self.weight = self.calc_ranking__standard(filters)
				self.important, self.weight = self.calc_ranking__quicklinks(filters)
			else:
				self.important, self.weight = self.calc_ranking__constant(filters)
		
		# propagate weight up tree, by adding our weight right up the tree...
		if propagate:
			parent = self.parItem
			while parent:
				parent.weightSum += self.weight
				parent = parent.parItem
	
	# ------------------------------------------
	
	# compute importance of node
	# - This is the part that we can vary to determine different importance effects
	#
	# < filters: (FilterModel)
	# > return[0]: is this node important or not
	# > return[1]: "weight" value which can be stored
	
	# Helper - account for filters when determining if something may potentially be important
	def calc_importance__filters(self, filters):
		# simply read this off from cache - assuming it has been updated first
		important = filters.folderOkOverview[self.data]
		
		# return importance according to filters
		return important
	
	# Ranking Mechanism: Constant (Baseline)
	# NOTE: this is the same as the linear condition (i.e. all items are equally important)
	def calc_ranking__constant(self, filters):
		# importance is still just in-range...
		t = self.data.mtime()
		inRange = (filters.tmin <= t <= filters.tmax)
		
		important = inRange and filters.folderOkOverview[self.data]
		
		# (isImportant, weight)
		return (important, 1.0)
		
	# Standard ranking mechanism - for unreadable scoft
	def calc_ranking__standard(self, filters):
		# current time
		t = self.data.mtime()                           # time for item - last modification time
		
		tmodel = filters.fs_model.time_model            # full time-range
		now = tmodel.tmax                               # "current" time (i.e. last timestamp)
		
		# weight
		recency = (now - t)                             # (0 = new) => (M = old)
		scaleFactor = 100000                            # controls how quickly it drops off
		weight = scaleFactor / (recency + scaleFactor)  # [0.0, 1.0]
		
		# recency drops off for less relevant, so only most relevant get highlighted this way
		# XXX: only select top N of these to highlight
		important = (weight > 0.1) and filters.folderOkOverview[self.data]
		
		# (isImportant, weight)
		return (important, weight)

################################################################


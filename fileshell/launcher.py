# Launcher is responsible for starting FileShell instance
# Author: Joshua Leung

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# main method for standalone FileShell
def main():
	from optparse import OptionParser

	# handle options
	usage = "Usage: %prog [-d database] path"
	parser = OptionParser(usage)
	
	DEFAULT_DB = ".fileshell/places.db"
	parser.add_option("-d", "--database", dest="db", 
			default=DEFAULT_DB, type="string",
			help="Path to database (DB) containing file system info")
	parser.add_option("-c", "--cleared", dest="cleared",
			action="store_true", default=False,
			help="Clear/reset time model, and don't show any recency hints")
	
	# parse commandline options
	# NOTE: this may exit if user just selected help...
	(options, args) = parser.parse_args()
	
	# extract relevnt parameters
	db   = options.db
	path = args[0] if len(args) else ""
	
	is_snapshot = (db is not DEFAULT_DB)	# i.e. user explicitly specified a snapshot to use
	cleared = options.cleared
	
	# only import everything now...
	import PyQt4 as pqt
	from PyQt4 import QtCore as qcore
	from PyQt4 import QtGui as qgui
	
	from fileshell import mainWindow
	from fileshell.model import fsdb
	
	# HACK: force all operating systems to use same graphics-painter subsystem
	# so that lines are rendered the same in Windows and Linux
	# (Note: must be set before creating app class)
	qgui.QApplication.setGraphicsSystem("raster")
	
	# set up application and its main window
	app = qgui.QApplication(sys.argv)	
	
	# load database
	fs_model = fsdb.FsModel(db, is_snapshot)
	fs_model.load(include_files=True, sort_data=True)
	
	# features to enable/disable
	features = {
		'use_jumps'         : True,
	}
	
	# set up main window
	win = mainWindow.FileShell(fs_model, path, features)
	sys.exit(app.exec_())
